package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SencillaNormalJPADAOTEST 
{
   
    //Fíjate son variables static de clase
    private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;


    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ISencillaNormalDAO idao=null;
    private static IEditorialDAO idaoe = null;
    private static ITipoObraDAO idaoto = null;


    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            Properties pro = new Properties();
            pro.load(SencillaColeccionJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
            bd=pro.getProperty("bdJDBC");
            login=pro.getProperty("loginJDBC");
            password=pro.getProperty("passwordJDBC");
            url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                       .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);

            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());


            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream("test-DAO-dataset.xml"));

            idao= new SencillaNormalJPADAO();
            idaoe = new EditorialJPADAO();
            idaoto = new TipoObraJPADAO();

        } catch (InstantiationException ex) {
                System.out.println("Hubo un problema al iniciar la base de datos del test: ");
                ex.printStackTrace();
        } catch (IllegalAccessException iae) {
                System.out.println("Hubo un problema al acceder a la base de datos. del test");
                iae.printStackTrace();
        } catch (SQLException sqle) {
                System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
                sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
                System.out.println("No se encuentra el driver JDBC."+cnfe);
                cnfe.printStackTrace();
        } catch (IOException ioe) {
                System.out.println("Error al accerder al archivo de configuración: "+ioe);
                ioe.printStackTrace();
        } catch (Exception ex) {
                System.out.println("Error inesperado al iniciar los test");
                ex.printStackTrace();
        }
    }
	

    @AfterClass
    public static  void finalizar() throws Exception
    {
        if(connection!=null)
            connection.close();

        idao.finalizar();
    }
	
	

    @Before
    public void setUp() throws Exception
    {
        DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }
		
	
	
    @After
    public void tearDown() throws Exception {
    }
	
	
    @Test
    public void testObtenerTodasSencillasNormales() 
    {
        try
        {
            List<SencillaNormal> sencillas= idao.obtenerTodasSencillas();
            assert(sencillas.size()==5);
            
            for(SencillaNormal sn :sencillas)
                    assertNotNull(sn.getISBN());

            SencillaNormal sn=sencillas.get(0);
                    
            assert(sn.getISBN().equals("441-55"));
            assert(sn.getNombre().equals("antonia"));
            assert(sn.getAutor().equals("paquito"));
            assert(sn.getIdioma().equals("chino"));
            assert(sn.getDerechos()==true);
            assert(sn.getEditorial().getCif().equals("2"));
            assert(sn.getTipo().getNombre().equals("tipoobra2"));
            assert(sn.getSubtipoObra().equals("chunga"));
            assert(sn.getDescripcion().equals("Mola mazo"));
            assert(sn.getPrecioIndividual()==5);

            SencillaNormal snor = null;

            for(SencillaNormal sn2 : sencillas)
                if(sn2.getISBN().equals("441-55"))
                    snor=sn2;

            assertNotNull(snor);
            assert(snor.getNombre().equals("antonia"));
            assertNotNull(snor.getEditorial());
            assertNotNull(snor.getTipo());

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de obras sencillas normales al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
	
	
    @Test
    public void testNuevaSencillaNormal() 
    {
        try
        {
            SencillaNormal sn= new SencillaNormal();
            
            List<SencillaNormal> sencillas = idao.obtenerTodasSencillas();
            long id= sencillas.get(sencillas.size()-1).getId()+1;
                
            sn.setId(id); 
            sn.setISBN("ISBNHP1");
            sn.setNombre("Harry Potter");
            sn.setAutor("JK Rowling");
            sn.setIdioma("Inglés");
            sn.setDerechos(true);

            Editorial e=idaoe.obtenerEditorial("1");
            sn.setEditorial(e);
            
            TipoObra tp = idaoto.obtenerTipoObra("tipoobra1");
            sn.setTipo(tp);
            
            sn.setSubtipoObra("Saga fantástica - magos");
            sn.setDescripcion("Harry Potter es cool y hace stuff como Jesús");
            sn.setPrecioIndividual(35);

            idao.nuevaSencillaNormal(sn);

            sencillas=idao.obtenerTodasSencillas();
            SencillaNormal snor=null;
            
            for(SencillaNormal sn2 :sencillas)
                    if(sn2.getISBN().equals("ISBNHP1"))
                            snor=sn2;

            assertNotNull(snor);
            assert(snor.getISBN().equals("ISBNHP1"));
            assert(snor.getNombre().equals("Harry Potter"));
            assert(snor.getAutor().equals("JK Rowling"));
            assert(snor.getIdioma().equals("Inglés"));
            assert(snor.getDerechos()==true);
            assert(snor.getEditorial().getCif().equals("1"));
            assert(snor.getTipo().getNombre().equals("tipoobra1"));
            assert(snor.getSubtipoObra().equals("Saga fantástica - magos"));
            assert(snor.getDescripcion().equals("Harry Potter es cool y hace stuff como Jesús"));
            assert(snor.getPrecioIndividual()==35);
            
        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha insertado la obra sencilla normal al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }

	
    @Test
    public void testObtenerEditorial() 
    {
        try
        {
            SencillaNormal sn=idao.obtenerSencillaNormal("441-55");

            if(sn.getISBN()==null)
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, la obra no tiene ISBN");

            if(!(sn.getNombre().equals("antonia")))
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, el nombre no es el correcto");

            if(!(sn.getAutor().equals("paquito")))
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, el autor no es el correcto");

            if(!(sn.getIdioma().equals("chino")))
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, el idioma no es el correcto");

            if(!(sn.getDerechos()==true))
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, los derechos no son los correctos");

            if(sn.getEditorial()==null)
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, la obra no tiene editorial");

            if(sn.getTipo()==null)
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, la obra no tiene tipo obra");

            if(!(sn.getSubtipoObra().equals("chunga")))
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, el subtipo de obra no es el correcto");
            
            if(!(sn.getDescripcion().equals("Mola mazo")))
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, la descripción no es la correcta");
            
            if(!(sn.getPrecioIndividual()==5))
                    fail("No se ha obtenido la obra sencilla normal apropiadamente, el precio de la obra no es el correcto");

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la obra sencilla normal al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
        
        
    @Test
    public void testModificaSencillaNormal() 
    {
        try
        {
            SencillaNormal sn= new SencillaNormal();

            sn.setNombre("Prueba");
            sn.setAutor("Prueba");
            sn.setIdioma("Español");
            sn.setDerechos(false);
            
            Editorial e = idaoe.obtenerEditorial("2");
            sn.setEditorial(e);
            
            TipoObra tp = idaoto.obtenerTipoObra("tipoobra2");
            sn.setTipo(tp);
            
            sn.setSubtipoObra("prueba");
            sn.setDescripcion("modifica_prueba");
            sn.setPrecioIndividual(20);
            
            idao.modificaSencillaNormal("441-55",sn);
                
            SencillaNormal sn_mod = idao.obtenerSencillaNormal("441-55");
                
            assert(sn_mod.getNombre().equals("Prueba"));
            assert(sn_mod.getAutor().equals("Prueba"));
            assert(sn_mod.getIdioma().equals("Español"));
            assert(sn_mod.getDerechos()==false);
            assert(sn_mod.getEditorial().getCif().equals("2"));
            assert(sn_mod.getTipo().getNombre().equals("tipoobra2"));
            assert(sn_mod.getSubtipoObra().equals("prueba"));
            assert(sn_mod.getDescripcion().equals("modifica_prueba"));
            assert(sn_mod.getPrecioIndividual()==20);
            

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido modificar la obra sencilla normal al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
	
    
    @Test
    public void testEliminaSencillaNormal() 
    {
        try
        {
            idao.eliminarSencillaNormal("442-55");
            
            List<SencillaNormal> sencillas=null;
            sencillas=idao.obtenerTodasSencillas();

            if(!(sencillas.size()==4))
                fail("TEST NO SUPERADO: No se ha eliminado la obra sencilla normal");

            if(idao.obtenerSencillaNormal("442-55")!=null)
                fail("TEST NO SUPERADO: No se ha eliminado la obra sencilla normal seleccionada");

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista de obras sencillas normales al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
}
