package com.lucassoft.editorialjpa.entidades;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity
@DiscriminatorValue("Sencilla")

public abstract class Sencilla extends Obra implements Serializable  {
	@Version
        private static final long serialVersionUID = 1L;
        
	private String archivo;

	public String getArchivo() {
		return archivo;
	}

	public void setArchivo(String archivo) {
		this.archivo = archivo;
	}

}
