/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.ClienteIndividualJPADAO;
import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.SencillaNormalJPADAO;
import com.lucassoft.editorialjpa.entidades.ClienteIndividual;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author user
 */
public class ServicioClienteIndividual {
    ClienteIndividualJPADAO idao=null;
    SencillaNormalJPADAO idaosn;
    
    public ServicioClienteIndividual() throws DAOException
	{
		idao=new ClienteIndividualJPADAO();
                idaosn = new SencillaNormalJPADAO();
	}
    
    public void nuevoClienteIndividual(String email, String password, String nombre, String pais, String provincia, String localidad, String codigopostal, String direccion, Date fechanacimiento, String[] array_obrasindividuales) throws ServicioException, DAOException
	{
		ClienteIndividual e = new ClienteIndividual();
        
                List<ClienteIndividual>clientes = idao.obtenerTodosClienteIndividual();
                    if(clientes.isEmpty())
                         e.setId(0);
                    else
                    {
                        long id= clientes.get(clientes.size()-1).getId()+1;
                        e.setId(id);
                    }
                
                
                
                
		e.setEmail(email);
		e.setPassword(password);
		e.setNombre(nombre);
		e.setPais(pais);
		e.setProvincia(provincia);
		e.setLocalidad(localidad);
		e.setCodigopostal(codigopostal);
		e.setDireccion(direccion);
		e.setFechaNacimiento(fechanacimiento);
                
                //Aquí va lo del campo lista
                
                List<SencillaNormal> obras=new ArrayList();
                
                for (int i = 0; i < array_obrasindividuales.length; i++) 
                {
                    SencillaNormal sn=idaosn.obtenerSencillaNormal(array_obrasindividuales[i]);

                    if (sn==null) 
                    {
                        throw new ServicioException("No existe una SencillaNormal con ese ISBN");  
                    }
                    else
                    {
                        obras.add(sn);
                    }                   
                }
                e.setObrasIndividuales(obras);
                
		ClienteIndividual ePrueba=idao.obtenerClienteIndividualEmail(email);
                
        if(ePrueba!=null)
            throw new ServicioException("Ya existe un cliente individual con ese email");
        
        ClienteIndividual ePruebaEmail=idao.obtenerClienteIndividualEmail(e.getEmail());
        if(ePruebaEmail!=null)
        	throw new ServicioException("Ya existe una clienteindividual con ese email");
        
        idao.nuevoClienteIndividual(e);
	}
    
    
    public ClienteIndividual obtenerClienteIndividual(String email) throws DAOException
	{
            ClienteIndividual c = idao.obtenerClienteIndividualEmail(email);

           return c;
        }
    
    
    public List<ClienteIndividual> obtieneClientesIndividuales() throws DAOException
	{
		 List<ClienteIndividual> clientesIndividuales= idao.obtenerTodosClienteIndividual();
	        
	        if(clientesIndividuales==null)
	        	clientesIndividuales=new ArrayList<ClienteIndividual>();
	        
	        return clientesIndividuales;
	}
    
    
    public void borraClienteIndividual()
	{
            //falta método borrar
	}
    
    
    public void modificaClienteIndividual(String email, String password, String nombre, String pais, String provincia, String localidad, String codigopostal, String direccion, Date fechanacimiento) throws DAOException, ServicioException
	{
		ClienteIndividual e = new ClienteIndividual();
		
		e.setEmail(email);
		e.setPassword(password);
		e.setNombre(nombre);
		e.setPais(pais);
		e.setProvincia(provincia);
		e.setLocalidad(localidad);
		e.setCodigopostal(codigopostal);
		e.setDireccion(direccion);
		e.setFechaNacimiento(fechanacimiento);
                
            ClienteIndividual ePrueba=idao.obtenerClienteIndividualEmail(email);
                    
            if(ePrueba==null)
                throw new ServicioException("No existe un cliente individual con ese email");
            
            int afectadas = idao.modificaClienteIndividual(email,e);
        
            if (afectadas==0)
        	throw new ServicioException("No es ha modificado ningun cliente individual.");
        
	}
    
        public void clienteobranueva( String isbn, String email) throws DAOException, ServicioException
        {
            ClienteIndividual clientei=idao.obtenerClienteIndividualEmail(email);
            
            if(clientei==null)
                throw new ServicioException("No existe un cliente individual con ese email");
            
            SencillaNormal sencillai = idaosn.obtenerSencillaNormal(isbn);
            
            if(sencillai==null)
            {
                throw new ServicioException("No existe una SencillaNormal con ese isbn");
            }
            
            List<SencillaNormal>sencillacliente = clientei.getObrasIndividuales();
            
            int cuenta = 0;
            
            for(SencillaNormal comprueba:sencillacliente)
                if(comprueba.getISBN().equals(sencillai.getISBN()))
                    {
                        cuenta++;
                        throw new ServicioException("Ya existe una SencillaNormal con ese isbn para ese cliete");
                    }
            
            if(cuenta==0)
                sencillacliente.add(sencillai);
                
        }
        
        public void clienteobraborrar( String isbn, String email) throws DAOException, ServicioException
        {
            
             ClienteIndividual clientei=idao.obtenerClienteIndividualEmail(email);
            
            if(clientei==null)
                throw new ServicioException("No existe un cliente individual con ese email");
            
            SencillaNormal sencillai = idaosn.obtenerSencillaNormal(isbn);
            
            if(sencillai==null)
            {
                throw new ServicioException("No existe una SencillaNormal con ese isbn");
            }
            
            List<SencillaNormal>sencillacliente = clientei.getObrasIndividuales();
            
            int cuenta = 0;
            
            for(SencillaNormal comprueba:sencillacliente)
                if(comprueba.getISBN().equals(sencillai.getISBN()))
                {
                    sencillacliente.remove(sencillai);
                }
                else
                {
                    throw new ServicioException("No existe una SencillaNormal con ese isbn");
                }
            
            
        }

        
    }