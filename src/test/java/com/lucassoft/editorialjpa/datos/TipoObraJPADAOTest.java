package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TipoObraJPADAOTest 
{
    private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ITipoObraDAO idao=null;
    private static EntityManagerFactory emf;
	
    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            idao= new TipoObraJPADAO();
            Properties pro = new Properties();
            pro.load(TipoObraJPADAOTest.class.getResourceAsStream("/configuracion.properties"));
            bd=pro.getProperty("bdJDBC");
            login=pro.getProperty("loginJDBC");
            password=pro.getProperty("passwordJDBC");
            url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                           .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);


            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());


            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
            .getContextClassLoader()
            .getResourceAsStream("test-DAO-dataset.xml"));

        } catch (InstantiationException ex) {
            System.out.println("Hubo un problema al iniciar la base de datos del test: ");
            ex.printStackTrace();
        } catch (IllegalAccessException iae) {
            System.out.println("Hubo un problema al acceder a la base de datos. del test");
            iae.printStackTrace();
        } catch (SQLException sqle) {
            System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
            sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            System.out.println("No se encuentra el driver JDBC."+cnfe);
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error al accerder al archivo de configuración: "+ioe);
            ioe.printStackTrace();   
        }catch (Exception ex) {
                System.out.println("Error inesperado al iniciar los test");
                ex.printStackTrace();
        }
    }
	
    @AfterClass
    public static  void finalizar() throws Exception{
            if(connection!=null){
                    connection.close();
            }
    }

    @Before
    public void setUp() throws Exception{
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNuevoTipoObra() 
    {
        try
        {
            TipoObra o1=new TipoObra();
            List<TipoObra>TipoObra = idao.obtenertodosTipoObra();
            long id= TipoObra.get(TipoObra.size()-1).getId()+1;
                    
            o1.setId(id);
            o1.setNombre("2Dam");
            o1.setDescripcion("2Dam");

            assert(o1.getNombre().equals("2Dam"));
            assert(o1.getDescripcion().equals("2Dam"));

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se han insertado el tipo de Obra al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }

    @Test
    public void testObtenerTodosTipoObra() 
    {
        try
        {
            List<TipoObra> tipoObras= idao.obtenertodosTipoObra();
            assert(tipoObras.size()==5);

            TipoObra a2=null;
            
            for(TipoObra to :tipoObras)
                    if(to.getNombre().equals("tipoobra2"))
                            a2=to;

            assertNotNull(a2);
            assert(a2.getNombre().equals("tipoobra2"));
            assert(a2.getDescripcion().equals("tipoobra2"));

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }


    @Test
    public void testObtenerTipoObra() 
    {
        try
        {
            TipoObra g=idao.obtenerTipoObra("tipoobra1");
            assert(g.getNombre().equals("tipoobra1"));
            assert(g.getDescripcion().equals("tipoobra1"));
            
        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista de equipos al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }


    @Test
    public void testEliminarTipoObra() 
    {
        try
        {
            idao.eliminarTipoObra("tipoobra1");

            List<TipoObra> tipos=null;
            tipos=idao.obtenertodosTipoObra();

            if(tipos.size()!=4)
                fail("TEST NO SUPERADO: No se ha eliminado el tipo de obra");

            if(idao.obtenerTipoObra("tipoobra1")!=null)
                fail("TEST NO SUPERADO: No se ha eliminado el tipo de obra Seleccionado");

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido obtener la lista de tipos de obra al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testModificaTipoObra() 
    {
        try
        {
            idao.modificaTipoObra("tipoobra1","obras de terror");
  
            TipoObra to=idao.obtenerTipoObra("tipoobra1");
            assert(to.getDescripcion().equals("obras de terror"));

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido modificar el tipo de obra al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
}
