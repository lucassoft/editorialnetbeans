/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;


import com.lucassoft.editorialjpa.entidades.TipoGrupo;
import java.util.List;

/**
 *
 * @author Abraham
 */
public interface ITipoGrupo {
    
    public void nuevoTipoGrupo(TipoGrupo tg) throws DAOException;
         public int eliminarTipoGrupo(String descripcion) throws DAOException;
	 public List<TipoGrupo> obtenertodosTipoGrupo() throws DAOException;
	 public TipoGrupo obtenerTipoGrupo(String descripcion) throws DAOException;
         public TipoGrupo obtenerTipoGrupo(long id) throws DAOException;
         public int modificaTipoGrupo(int maximoComponentes, String descripcion) throws DAOException;
         public void finalizar() throws DAOException; 
    
}
