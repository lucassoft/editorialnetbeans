/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.ClienteIndividual;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author kiko
 */
public class ClienteIndividualJPADAOTEST {
    
    
	//Fíjate son variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static IClienteIndividualDAO idao=null;
	
	
	//Este método se ejucuta UNA vez, justo antes de empezar las pruebas
	@BeforeClass
	public static void inicializarBD() throws Exception {
		

        try {
       	 //Ojo esta es una clase de test obtendrá la configuración
        //del .properties de src/TEST/resources NO de src/main/resources
       	 Properties pro = new Properties();
   		 pro.load(ClienteIndividualJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

       	 
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	         idao= new ClienteIndividualJPADAO();
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
	
	//Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
                //Esto es nuevo hay que cerrar la factoria de sesiones
		idao.finalizar();
	}
	
	
	//Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
	
	//Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	
	
	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void testObtenerTodasClienteIndividual() {
		try{
                        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
			Date d= sdf.parse("1998-3-25");
			List<ClienteIndividual> clientes= idao.obtenerTodosClienteIndividual();
			
			assert(clientes.size()==5);
			
			for(ClienteIndividual ci :clientes)
				assertNotNull(ci.getEmail());
			
			ClienteIndividual ci=clientes.get(0);
			
                        assert(ci.getNombre().equals("ed1"));
                        assert(ci.getPassword().equals("1"));	
			assert(ci.getEmail().equals("6@email.com"));					
			assert(ci.getPais().equals("pais"));
			assert(ci.getProvincia().equals("p1"));
			assert(ci.getLocalidad().equals("l1"));
			assert(ci.getCodigopostal().equals("1"));
			assert(ci.getDireccion().equals("d1"));
			assert(ci.getFechaNacimiento().equals(d));
			
			ClienteIndividual cli = null;
			
			for(ClienteIndividual ci2 : clientes)
				if(ci2.getEmail().equals("6@email.com"))
					cli=ci;
			
			assertNotNull(cli);
			assert(cli.getNombre().equals("ed1"));
			assertNotNull(cli.getEmail());
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de Cliente Individual al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	
	
	@Test
	public void testNuevoClienteIndividual() {
		try{
                    
                List<ClienteIndividual>clientes = idao.obtenerTodosClienteIndividual();
                long id= clientes.get(clientes.size()-1).getId()+1;
                    
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
		Date d= sdf.parse("1998-03-25");
		ClienteIndividual ci1= new ClienteIndividual();
			
                ci1.setId(id);
		ci1.setEmail("prueba@email.com");
		ci1.setPassword("prueba");
		ci1.setNombre("EditorialKiko");
		ci1.setProvincia("11111111L");
	        ci1.setLocalidad("kiko");
	        ci1.setCodigopostal("619154672");
	        ci1.setDireccion("965479079");
	        ci1.setFechaNacimiento(d);
	        ci1.setPais("España");
			
			idao.nuevoClienteIndividual(ci1);
			
			//List<ClienteIndividual> clientes=idao.obtenerTodosClienteIndividual();
			
                        clientes=idao.obtenerTodosClienteIndividual();
                        
			ClienteIndividual ci2=null;
			for(ClienteIndividual c :clientes)
				if(c.getEmail().equals("prueba@email.com"))
					ci2=c;
				
			
			assertNotNull(ci2);
			assert(ci2.getEmail().equals("prueba@email.com"));
			assert(ci2.getPassword().equals("prueba"));
			assert(ci2.getNombre().equals("EditorialKiko"));
			assert(ci2.getProvincia().equals("11111111L"));
			assert(ci2.getLocalidad().equals("kiko"));
			assert(ci2.getCodigopostal().equals("619154672"));
			assert(ci2.getDireccion().equals("965479079"));
			assert(ci2.getFechaNacimiento().equals(d));
			assert(ci2.getPais().equals("España"));
			
			
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha insertado la editorial al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}

	
	@Test
	public void testObtenerClienteIndividual() {
		
		try
		{
			ClienteIndividual ci=idao.obtenerClienteIndividualEmail("6@email.com");
			
			if(ci.getEmail()==null)
				fail("No se ha obtenido El cliente individual apropiadamente, la editorial no tiene email");
			
			if(!(ci.getPassword().equals("1")))
				fail("No se ha obtenido El cliente individual apropiadamente, la contraseña no es la correcta");
			
			if(!(ci.getNombre().equals("ed1")))
				fail("No se ha obtenido El cliente individual apropiadamente, el nombre no es el correcto");
			
			if(!(ci.getProvincia().equals("p1")))
				fail("No se ha obtenido El cliente individual apropiadamente, la editorial no tiene provincia");
			
			if(!(ci.getLocalidad().equals("l1")))
				fail("No se ha obtenido El cliente individual apropiadamente, la editorial no tiene Localidad");
			
			if(!(ci.getCodigopostal().equals("1")))
				fail("No se ha obtenido El cliente individual apropiadamente, el Codigo Postal no es el correcto");
			
			if(!(ci.getDireccion().equals("d1")))
				fail("No se ha obtenido El cliente individual apropiadamente, la direccion no es el correcto");
			
                        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
                        Date d= sdf.parse("1998-03-25");
			if(!(ci.getFechaNacimiento().equals(d)))
				fail("No se ha obtenido el cliente apropiadamente, la fecha no es la correcta");
			
			if(!(ci.getPais().equals("pais")))
				fail("No se ha obtenido El cliente individual apropiadamente, el país no es el correcto");
			
			
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de cliente individual al producirse una excepcion: "+e.getLocalizedMessage());
                }
	}
	

	@Test
	public void testEliminaClienteIndividual() {
		/*	try{
		//Antes de empezar a comprobar 
		//Por qué no se pasa este test (en caso de que no lo haga
		//Nos aseguraremos de que ObternerTodosJugador y obtenerJugador funciona bien
		
			
		List<Alumno> alumnos=null;
		
		idao.eliminarAlumno(2);
		alumnos=idao.obtenerTodosAlumnos();
		if(!(alumnos.size()==4)){
			fail("TEST NO SUPERADO: No se ha eliminado el alumno");
		}
		if(idao.obtenerAlumno(2)!=null){
			fail("TEST NO SUPERADO: No se ha eliminado el alumno Seleccionado");
		}
		
		
		
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de alumnos al producirse una excepcion: "+e.getLocalizedMessage());
		}*/
	}

}
