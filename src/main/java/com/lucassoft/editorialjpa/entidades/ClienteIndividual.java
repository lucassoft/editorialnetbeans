package com.lucassoft.editorialjpa.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.Version;

@Entity
@DiscriminatorValue("ClienteIndividual")

public class ClienteIndividual extends Cliente implements Serializable {
    
    @Version
    private static final long serialVersionUID = 1L;
	
        
        @Temporal(javax.persistence.TemporalType.DATE)
        protected Date fechaNacimiento;
        //protected Subgrupo subgrupos;
        
        @ManyToMany
	private List<SencillaNormal> Obrasindividuales;

        
    public Date getFechaNacimiento() 
    {
        return fechaNacimiento;
    }
    
    public void setFechaNacimiento(Date fechaNacimiento) 
    {
        this.fechaNacimiento = fechaNacimiento;
    }
    
    public List<SencillaNormal> getObrasIndividuales()
    {
        return Obrasindividuales;
    }
    
    public void setObrasIndividuales(List<SencillaNormal> obrasindividuales) 
    {
		Obrasindividuales = obrasindividuales;
    }
    
        
	

}
