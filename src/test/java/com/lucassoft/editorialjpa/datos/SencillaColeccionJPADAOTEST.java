package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.SencillaColeccion;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SencillaColeccionJPADAOTEST
{
	
    private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ISencillaColeccionDAO idao=null;
    private static IEditorialDAO idaoe=null;
    private static ITipoObraDAO idaoto=null;
    private static Connection jdbcConnection =null;


    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {

            Properties pro = new Properties();
            pro.load(SencillaColeccionJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
            bd=pro.getProperty("bdJDBC");
            login=pro.getProperty("loginJDBC");
            password=pro.getProperty("passwordJDBC");
            url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
                        jdbcConnection = (Connection) DriverManager
                       .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);

            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                  .getContextClassLoader()
                  .getResourceAsStream("test-DAO-dataset.xml"));

            idao= new SencillaColeccionJPADAO();
            idaoe= new EditorialJPADAO();
            idaoto= new TipoObraJPADAO();

        } catch (InstantiationException ex) {
            System.out.println("Hubo un problema al iniciar la base de datos del test: ");
            ex.printStackTrace();
        } catch (IllegalAccessException iae) {
            System.out.println("Hubo un problema al acceder a la base de datos. del test");
            iae.printStackTrace();
        } catch (SQLException sqle) {
            System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
            sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            System.out.println("No se encuentra el driver JDBC."+cnfe);
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error al accerder al archivo de configuración: "+ioe);
            ioe.printStackTrace();
        } catch (Exception ex) {
            System.out.println("Error inesperado al iniciar los test");
            ex.printStackTrace();
        }
    }

    @AfterClass
    public static  void finalizar() throws Exception
    {
        if(connection!=null)
            connection.close();

        idao.finalizar();
    }


    @Before
    public void setUp() throws Exception
    {
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
    }


    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testObtenerTodasSencillaColeccion() 
    {
        try
        {
            List<SencillaColeccion> sencillasc= idao.obtenerTodasSencillaColeccion();

            assert(sencillasc.size()==5);

            for(SencillaColeccion e :sencillasc)
                    assertNotNull(e.getISBN());

            SencillaColeccion sc=sencillasc.get(0);

            assert(sc.getISBN().equals("446-55"));
            assert(sc.getNombre().equals("antonia"));
            assert(sc.getAutor().equals("paquito"));
            assert(sc.getIdioma().equals("chino"));
            assert(sc.getDerechos()==true);
            assert(sc.getEditorial().getCif().equals("2"));
            assert(sc.getTipo().getNombre().equals("tipoobra2"));
            assert(sc.getSubtipoObra().equals("chunga"));
            assert(sc.getDescripcion().equals("Mola mazo"));
            assert(sc.getArchivo().equals("1"));


            SencillaColeccion scol = null;

            for(SencillaColeccion sc2 : sencillasc)
                    if(sc2.getISBN().equals("446-55"))
                            scol=sc2;

            assertNotNull(scol);
            assert(scol.getNombre().equals("antonia"));
            assertNotNull(scol.getEditorial());
            assertNotNull(scol.getTipo());

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de obras sencillas de colección al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }


    @Test
    public void testNuevaSencillaColeccion() 
    {
        try
        {
            SencillaColeccion sc= new SencillaColeccion();
            
            List<SencillaColeccion> sencillasc = idao.obtenerTodasSencillaColeccion();
            long id= sencillasc.get(sencillasc.size()-1).getId()+1;
                
            sc.setId(id); 
            sc.setISBN("44-551");
            sc.setNombre("prueba");
            sc.setAutor("El Pollo");
            sc.setIdioma("Chino");
            sc.setDerechos(true);
            
            Editorial e=idaoe.obtenerEditorial("1");
            sc.setEditorial(e);
            
            TipoObra tp = idaoto.obtenerTipoObra("tipoobra1");
            sc.setTipo(tp);

            sc.setSubtipoObra("Drama");
            sc.setDescripcion("El mejor");
            sc.setArchivo("1234");

            idao.nuevaSencillaColeccion(sc);

            List<SencillaColeccion> todasColecciones=idao.obtenerTodasSencillaColeccion();
            SencillaColeccion scol=null;
            
            for(SencillaColeccion sn2 :todasColecciones)
                    if(sn2.getISBN().equals("44-551"))
                            scol=sn2;

            assertNotNull(scol);
            assert(scol.getISBN().equals("44-551"));
            assert(scol.getNombre().equals("prueba"));
            assert(scol.getAutor().equals("El Pollo"));
            assert(scol.getIdioma().equals("Chino"));
            assert(scol.getDerechos()==true);
            assert(scol.getEditorial().getCif().equals("1"));
            assert(scol.getTipo().getNombre().equals("tipoobra1"));
            assert(scol.getSubtipoObra().equals("Drama"));
            assert(scol.getDescripcion().equals("El mejor")); 
            assert(scol.getArchivo().equals("1234"));

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha insertado la sencilla coleccion al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }


    @Test
    public void testObtenerSencillaColeccion() 
    {
        try
        {
            SencillaColeccion sc=idao.obtenerSencillaColeccion("446-55");

            if(!(sc.getISBN().equals("446-55")))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, el cif no es la correcta");

            if(!(sc.getNombre().equals("antonia")))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, el nombre no es el correcto");

            if(!(sc.getAutor().equals("paquito")))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, la editorial no tiene cif");

            if(!(sc.getIdioma().equals("chino")))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, no el el idioma correcto");

            if(!(sc.getDerechos()==true))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, el derecho no es el correcto");
            
             if(sc.getEditorial()==null)
                fail("No se ha obtenido la sencillaColeccion apropiadamente, la obra no tiene editorial");

            if(sc.getTipo()==null)
                fail("No se ha obtenido la sencillaColeccion apropiadamente, la obra no tiene tipo obra");
            
            if(!(sc.getSubtipoObra().equals("chunga")))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, el subtipo no es el correcto");

            if(!(sc.getDescripcion().equals("Mola mazo")))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, la descripcion no es el correcto");
            
            if(!(sc.getArchivo().equals("1")))
                fail("No se ha obtenido la sencillaColeccion apropiadamente, la descripcion no es el correcto");

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista de SencillaColeccion al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }


    @Test
    public void testEliminaSencillaColeccion() 
    {
        try
        {
            List<SencillaColeccion> sencillaColecciones=null;
            idao.eliminarSencillaColeccion("447-55");
            sencillaColecciones=idao.obtenerTodasSencillaColeccion();

            if(!(sencillaColecciones.size()==4))
                fail("TEST NO SUPERADO: No se ha eliminado la SencillaColeccion");

            if(idao.obtenerSencillaColeccion("447-55")!=null)
                fail("TEST NO SUPERADO: No se ha eliminado la SencillaColeccion seleccionada");

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista de SencillaColecciones al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }

    @Test
    public void testModificaSencillaColeccion() 
    {
        try
        {
            SencillaColeccion sc = new SencillaColeccion();

            sc.setNombre("Prueba");
            sc.setAutor("Prueba");
            sc.setIdioma("Español");
            sc.setDerechos(false);
            
            Editorial e = idaoe.obtenerEditorial("2");
            sc.setEditorial(e);
            
            TipoObra tp = idaoto.obtenerTipoObra("tipoobra2");
            sc.setTipo(tp);
            
            sc.setSubtipoObra("prueba");
            sc.setDescripcion("modifica_prueba");
            sc.setArchivo("kiko");
            
            idao.modificaSencillaColeccion("449-55",sc);
                
            SencillaColeccion sc_mod = idao.obtenerSencillaColeccion("449-55");
                
            assert(sc_mod.getNombre().equals("Prueba"));
            assert(sc_mod.getAutor().equals("Prueba"));
            assert(sc_mod.getIdioma().equals("Español"));
            assert(sc_mod.getDerechos()==false);
            assert(sc_mod.getEditorial().getCif().equals("2"));
            assert(sc_mod.getTipo().getNombre().equals("tipoobra2"));
            assert(sc_mod.getSubtipoObra().equals("prueba"));
            assert(sc_mod.getDescripcion().equals("modifica_prueba"));
            assert(sc_mod.getArchivo().equals("kiko"));
            

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido modificar la sencillacolecion al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
}

