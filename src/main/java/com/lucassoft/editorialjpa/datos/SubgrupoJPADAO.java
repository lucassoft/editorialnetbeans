/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Coleccion;
import com.lucassoft.editorialjpa.entidades.SubGrupo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author saul
 */
public class SubgrupoJPADAO implements ISubGrupoDAO{
    
      EntityManagerFactory emf=null;
      
      
       public SubgrupoJPADAO() throws DAOException 
    {
        try
        {
            emf= Emf.getInstance().getEmf();
            
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        } 
    }
       
       
       public void nuevaSubGrupo(String nombre) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try 
        {
            em.persist(nombre);
            em.getTransaction().commit();
            
        } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar el subgrupo", ex);
        } finally {
            em.close();  
        } 
    }
       
       public SubGrupo obtenerSubGrupo(String nombre) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();
                
            Query query = em.createQuery("SELECT sn FROM SubGrupo sn WHERE sn.nombre=:nombre");
            query.setParameter("nombre",nombre);
            List subgrupo=query.getResultList();
            List<SubGrupo> SubGr=(List<SubGrupo>) subgrupo;
            
            if(SubGr.isEmpty())
                return null;
            
            return SubGr.get(0);
                      
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el subgrupo", ex);
        } finally {
            em.close(); 
        } 
    }
       
       
       public List<SubGrupo> obtenerTodasSubGrupo() throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT sn FROM SubGrupo sn ");
            
            List subgrupo = query.getResultList();
            return subgrupo;
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener los subgrupos", ex);
        } finally {
            em.close();
        }
    }
       
       
       
       public void eliminarSubGrupo(String nombre) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try
        {
            SubGrupo sn=em.find(SubGrupo.class,nombre);

            em.remove(sn);
            em.getTransaction().commit();
         
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al eliminar el subgrupo", ex);
        } finally {
            em.close();
        }
    }
       
       
       
       public int modificaSubGrupo(String nombre) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        try
        {
            em.getTransaction().begin();
            em.flush();

            SubGrupo identSN=this.obtenerSubGrupo(nombre);
            long id = identSN.getId();

            SubGrupo subgrupo = em.find(SubGrupo.class,id);
            
            
            

            /*subgrupo.setId(nombre.getId());
            subgrupo.setNombre(nombre.getNombre());
            subgrupo.setGrupo(nombre.getGrupo());
            subgrupo.setTipo(nombre.getTipo());*/
            
            

            em.persist(subgrupo);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el subgrupo", ex);  
        } finally {
            em.close(); 
        }
    } 
    
       public void finalizar() throws DAOException 
    {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }

    
}
