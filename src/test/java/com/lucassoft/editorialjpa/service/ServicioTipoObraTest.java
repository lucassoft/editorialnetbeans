package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.TipoObraJPADAOTest;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.lucassoft.editorialjpa.service.ServicioTipoObra;
import static org.junit.Assert.*;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import com.mysql.jdbc.Connection;
import org.junit.Test;

public class ServicioTipoObraTest 
{
    private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ServicioTipoObra sf=null;

    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            sf= new ServicioTipoObra();
            
            Properties pro = new Properties();
            pro.load(ServicioTipoObraTest.class.getResourceAsStream("/configuracion.properties"));
            bd=pro.getProperty("bdJDBC");
            login=pro.getProperty("loginJDBC");
            password=pro.getProperty("passwordJDBC");
            url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                                        .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            
            sf= new ServicioTipoObra();
            
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("test-DAO-dataset.xml"));
              
        } catch (InstantiationException ex) {
            System.out.println("Hubo un problema al iniciar la base de datos del test: ");
            ex.printStackTrace();
        } catch (IllegalAccessException iae) {
            System.out.println("Hubo un problema al acceder a la base de datos. del test");
            iae.printStackTrace();
        } catch (SQLException sqle) {
            System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
            sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            System.out.println("No se encuentra el driver JDBC."+cnfe);
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error al accerder al archivo de configuración: "+ioe);
            ioe.printStackTrace();
        }catch (Exception ex) {
            System.out.println("Error inesperado al iniciar los test");
            ex.printStackTrace();
        }
    }

    @AfterClass
    public static  void finalizar() throws Exception{
            if(connection!=null){
                    connection.close();
            }
    }

    @Before
    public void setUp() throws Exception{
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }

    @After
    public void tearDown() throws Exception {
    }

    
    @Test
    public void testNuevoTipoObra()
    {
        try
        {
            sf.nuevoTipoObra("molomuc.com", "macarron");
            List<TipoObra> tipoobra = sf.obtenerTodosTipoObra();
            
            if (tipoobra.size() != 6) 
                fail("No se ha insertado el TipoObra");
            

            for (TipoObra t : tipoobra) 
            {
                assertNotNull(t.getNombre());
                assertNotNull(t.getDescripcion());
            }
                
        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se ha insertado el TipoObra al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }

    @Test(expected = ServicioException.class)
    public void testNuevoTipoObraConNombreRepetidoFail() throws Exception 
    {
        sf.nuevoTipoObra("tipoobra1", "tipoobra1");
    }

    
    @Test
    public void testObtenerTipoObra()
    {
        try 
        {
            TipoObra to = sf.obtenerTipoObra("tipoobra1");
            
            assertNotNull(to.getDescripcion());
            
        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se han obtenido los tipoObra al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }
    
  
    @Test
    public void testObtenerTodosTipoObra()
    {
        try 
        {
            List<TipoObra> tiposobra = sf.obtenerTodosTipoObra();
            
            if (tiposobra.size() != 5) 
                    fail("No se han obtenido todas los tipoObra");
            
            
            for (TipoObra t : tiposobra) 
            {
                    assertNotNull(t.getNombre());
                    assertNotNull(t.getId());
                    assertNotNull(t.getDescripcion());
            }
            
        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se han obtenido los tipoObra al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }
    
    
    
    @Test   
    public void testElimitarTipoObra()
    {        
        try 
        {
            sf.borraTipoObra("tipoobra1");
            List<TipoObra> tipoobra = sf.obtenerTodosTipoObra();
            
            if (tipoobra.size() != 4) 
                    fail("No se ha eliminado el tipoObra");

            for (TipoObra t : tipoobra) 
            {
                assertNotNull(t.getNombre());
                assertNotNull(t.getId());
                assertNotNull(t.getDescripcion());
            }
            
        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se ha eliminado el TipoObra al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }
    
    
    @Test   
    public void testModificaTipoObra()
    {        
        try 
        {
            sf.modificaTipoObra("tipoobra1","terror");
            TipoObra to = sf.obtenerTipoObra("tipoobra1");
            
            assert(to.getDescripcion().equals("terror"));

        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se ha eliminado el TipoObra al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }
}

