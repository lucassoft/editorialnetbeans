package com.lucassoft.editorialjpa.service;

import java.util.ArrayList;
import java.util.List;
import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.EditorialJPADAO;
import com.lucassoft.editorialjpa.datos.SencillaNormalJPADAO;
import com.lucassoft.editorialjpa.datos.TipoObraJPADAO;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServicioSencillaNormal 
{
    long contador;
    SencillaNormalJPADAO idao=null;
    EditorialJPADAO idaoe=null;
    TipoObraJPADAO idaoto=null;
    
	public ServicioSencillaNormal() throws DAOException
	{
		idao=new SencillaNormalJPADAO();
                idaoe=new EditorialJPADAO();
                idaoto=new TipoObraJPADAO();  
	}
        
	
	public void nuevaSencillaNormal(String isbn, String nombre, String autor, String idioma, boolean derechos,String cif, String nombreTipoObra, String subTiObra, String descrip, float precio) throws ServicioException, DAOException
	{
            SencillaNormal sn = new SencillaNormal();
	
            List<SencillaNormal> sencillaNormal = idao.obtenerTodasSencillas();
            if(sencillaNormal.isEmpty())
                sn.setId(0);
            else
            {
                long id= sencillaNormal.get(sencillaNormal.size()-1).getId()+1;
                sn.setId(id);
            }
            
            sn.setISBN(isbn);
            sn.setNombre(nombre);
            sn.setAutor(autor);
            sn.setIdioma(idioma);
            sn.setDerechos(derechos);
            
            Editorial e = idaoe.obtenerEditorial(cif);
            
                if(e==null)
                    throw new ServicioException("No existe ningúna editorial con ese cif");
            
            sn.setEditorial(e);
            
            TipoObra to = idaoto.obtenerTipoObra(nombreTipoObra);
            
                if(to==null)
                    throw new ServicioException("No existe ningún tipo de obra con ese nombre");
            
            sn.setTipo(to);
                      
            sn.setSubtipoObra(subTiObra);
            sn.setDescripcion(descrip);
            sn.setPrecioIndividual(precio);
	 	
            SencillaNormal oPrueba=idao.obtenerSencillaNormal(isbn);
            
            if(oPrueba!=null)
                throw new ServicioException("Ya existe una obra con ese ISBN");
        
            idao.nuevaSencillaNormal(sn);
	}
	

        
	public SencillaNormal obtenerSencillaNormal(String isbn) throws DAOException, ServicioException
	{ 
            SencillaNormal sn= idao.obtenerSencillaNormal(isbn);
	        
	    if(sn==null)
                    throw new ServicioException("No existe ningúna obra con ese isbn");    
	        
	    return sn;
	}
	
        
        
        
	public List<SencillaNormal> obtenerTodasSencillas() throws DAOException
	{
            List<SencillaNormal> sencillas= idao.obtenerTodasSencillas();

            if(sencillas==null)
                sencillas=new ArrayList<SencillaNormal>();

            return sencillas;
	}

        
        public List<SencillaNormal> obtenerTodasSencillasEditorial(String cif) throws DAOException
	{
            List<SencillaNormal> sencillas= idao.obtenerTodasSencillasEditorial(cif);

            if(sencillas==null)
                sencillas=new ArrayList<SencillaNormal>();

            return sencillas;
	}
	
	
	public void eliminaSencillaNormal(String isbn) throws ServicioException, DAOException
	{
            try 
            {
                SencillaNormal sn = idao.obtenerSencillaNormal(isbn);

                if (sn==null)
                    throw new ServicioException("La obra sencilla normal no existe.");

                int resultado = idao.eliminarSencillaNormal(isbn);

                if (resultado!=1)
                    throw new ServicioException("No se ha eliminado la obra sencilla normal con isbn "+ isbn);

            } catch (ServicioException ex) {
                Logger.getLogger(ServicioSencillaNormal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
            
	
	
	
	public void modificaSencillaNormal(String isbn, String nombre, String autor, String idioma, boolean derechos,String cif, String nombreTipoObra, String subTiObra, String descrip,float precio) throws DAOException, ServicioException
	{
            SencillaNormal sn = new SencillaNormal();
		
            sn.setNombre(nombre);
            sn.setAutor(autor);
            sn.setIdioma(idioma);
            sn.setDerechos(derechos);
            
            Editorial e = idaoe.obtenerEditorial(cif);
            
                if(e==null)
                    throw new ServicioException("No existe ningúna editorial con ese cif");
            
            sn.setEditorial(e);
            
            TipoObra to = idaoto.obtenerTipoObra(nombreTipoObra);
            
                if(to==null)
                    throw new ServicioException("No existe ningún tipo de obra con ese nombre");
            
            sn.setTipo(to);
                      
            sn.setSubtipoObra(subTiObra);
            sn.setDescripcion(descrip);
            sn.setPrecioIndividual(precio);
                
            SencillaNormal snPrueba=idao.obtenerSencillaNormal(isbn);
                    
            if(snPrueba==null)
                throw new ServicioException("No existe una obra con ese isbn");
            
            int afectadas = idao.modificaSencillaNormal(isbn,sn);
        
            if (afectadas==0)
        	throw new ServicioException("No es ha modificado ninguna obra.");
	}
}
