package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import java.util.List;

public interface ISencillaNormalDAO 
{    
    public void nuevaSencillaNormal(SencillaNormal sn) throws DAOException;
    public SencillaNormal obtenerSencillaNormal(String isbn) throws DAOException;
    public List<SencillaNormal> obtenerTodasSencillas() throws DAOException;
    public List<SencillaNormal> obtenerTodasSencillasEditorial(String cif) throws DAOException;
    public int eliminarSencillaNormal(String isbn) throws DAOException; 
    public int modificaSencillaNormal(String isbn, SencillaNormal sn) throws DAOException;
    public void finalizar() throws DAOException;
}
