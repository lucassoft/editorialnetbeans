package com.lucassoft.editorialjpa.datos;

import java.util.List;

import com.lucassoft.editorialjpa.entidades.SencillaColeccion;

public interface ISencillaColeccionDAO 
{
	public void nuevaSencillaColeccion(SencillaColeccion s) throws DAOException;
	public SencillaColeccion obtenerSencillaColeccion(String ISBN) throws DAOException;
	public void eliminarSencillaColeccion(String ISBN) throws DAOException;
	public List<SencillaColeccion> obtenerTodasSencillaColeccion() throws DAOException;
	public int modificaSencillaColeccion(String ISBN,SencillaColeccion s) throws DAOException;
        public List<SencillaColeccion> obtenerTodasSencillasColeccionEditorial(String cif) throws DAOException;
        public void finalizar() throws DAOException ;
}
