/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.TipoObraJPADAOTest;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Kraftwerk
 */

/*public class ServicioObraTest {
    //Fíjate son variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static ServicioObra sf=null;
	
	//Vamos a probar la clase de servicio usando la implementación de JDBC
	//que es la única que va a tener test
	//como es lógico hasta que no se pasen todos los test de JDBCDAO
	//No deberemos empezar a preguntarnos por qué fallan estos otros
	//q seguro q lo haran
	
	//Este método se ejucuta UNA vez, justo antes de empezar las pruebas
	@BeforeClass
	public static void inicializarBD() throws Exception {
            
            
            
        try {
       	 //Ojo esta es una clase de test obtendrá la configuración
        //del .properties de src/TEST/resources NO de src/main/resources
         sf= new ServicioObra();
       	 Properties pro = new Properties();
   		 pro.load(ObraJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            sf= new ServicioObra();
            
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	      sf= new ServicioObra();
	      //sf.elegirSistemaAlmacenamiento(5);
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
        
        //Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
	}
        
        //Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
        
        //Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	@After
	public void tearDown() throws Exception {
	}
	
	
	//Anotamos con @Test todos los test que queramos hacer, generalmnte uno
	//por método.Antes de iniciar el test la bd está en un estado conocido
	//es decir la base de datos del test está tal y como la ha configurado el dataset-dao.xml
	//tras su ejecución por parte de dbunit
	//Estado Inicial 3 equipos  con 5 jugadores
	//1 Real Madrid 1Cristano 25/3/1998
	//				2Bale	
	//				3Keylor
	//2 FC Barcelona
	//				4 Paquete n1 2/2/1995
	//				5 Paquete n2 4/3/1997
	//3 Valenca CF
        
        @Test
	public void testNuevoObra(){
			
			try {
				
				sf.nuevoObra("9788423342310","1984", "Geroge Orwell", "Español", true, "Destino","Narrativa","Politica","jsljsdf");
                            List<Obra> obra = sf.obtenerTodosObra();
				if (obra.size() != 6) {//Hay que crear 5 editoriales de ejemplo
					fail("No se ha insertado la Obra");
				}
				for (Obra t : obra) {
                                       
                                        assertNotNull(t.getID());
                                        assertNotNull(t.getISBN());
                                        assertNotNull(t.getNombre());
                                        assertNotNull(t.getAutor());
                                        assertNotNull(t.getIdioma());
                                        assertNotNull(t.isDerechos());
                                        assertNotNull(t.getEditorial());
                                        assertNotNull(t.getTipo());
                                        assertNotNull(t.getSubtipoObra());
                                        assertNotNull(t.getDescripcion());
					
					
                                      
					
				}

			} catch (Exception e) {
				fail("TEST NO SUPERADO: No se ha insertado la Obra al producirse una excepcion: "
						+ e.getLocalizedMessage());
			}
			
			
		}
        
        //ESTE TEST FALTA
        
		//public void testModificarObra(){}


		/*@Test(expected = ServicioException.class)
		public void testNuevoObraConNombreRepetidoFail() throws Exception {
			
			
			sf.nuevoObra("molomuc@gmail.com", "macarron");

		}*/
		
		
		//VOY POR ESTE, QUE ESTÁ A MITAD
	/*	@Test
		public void testObtenerTodosObra(){
			
			try {

				List<Obra> obra = sf.obtenerTodosObra();
				if (obra.size() != 5) {
					fail("No se han obtenido todas los Obra");
				}
				for (Obra t : obra) {
					
                                    assertNotNull(t.getID());
                                        assertNotNull(t.getISBN());
                                        assertNotNull(t.getNombre());
                                        assertNotNull(t.getAutor());
                                        assertNotNull(t.getIdioma());
                                        assertNotNull(t.isDerechos());
                                        assertNotNull(t.getEditorial());
                                        assertNotNull(t.getTipo());
                                        assertNotNull(t.getSubtipoObra());
                                        assertNotNull(t.getDescripcion());
					
				}
			} catch (Exception e) {
				fail("TEST NO SUPERADO: No se han obtenido la Obra al producirse una excepcion: "
						+ e.getLocalizedMessage());
			}
			
			
		}
		
                
                @Test   
		public void testElimitarObra(){
			/*
			try {
				//¿Que ocurre si decidimos eliminar un jugador que no existe o con id negativo?
				//según esta implementación no se hace nada, ya que no añadimos un test fail, para estos casos
				sf.borraTipoObra("tipoobra1");
				List<TipoObra> tipoobra = sf.obtenerTodosTipoObra();
				if (tipoobra.size() != 4) {
					fail("No se ha eliminado el tipoObra");
				}
				for (TipoObra t : tipoobra) {
					assertNotNull(t.getNombre());
					assertNotNull(t.getId());
					assertNotNull(t.getDescripcion());
					
				}
				
			} catch (Exception e) {
				fail("TEST NO SUPERADO: No se ha eliminado el TipoObra al producirse una excepcion: "
						+ e.getLocalizedMessage());
			}*/
		//}

    
//}
