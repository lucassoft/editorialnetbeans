/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Grupo;
import java.util.List;

/**
 *
 * @author Abraham
 */
public interface IGrupoDAO {
    
        public void nuevoGrupo(Grupo g) throws DAOException;
        public void finalizar() throws DAOException;
	public Grupo obtenerGrupo(long id) throws DAOException;
	public Grupo obtenerGrupoEmail(String email) throws DAOException;
	public int eliminarGrupo(String email) throws DAOException;
	public List<Grupo> obtenerTodosGrupos() throws DAOException;
	public int modificaGrupo(String email, Grupo g ) throws DAOException;
    
}
