/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.TipoGrupo;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Abraham
 */
public class TipoGrupoJPADAOTEST {
    
      private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ITipoGrupo idao=null;
    private static EntityManagerFactory emf;
    
      @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            idao= new TipoGrupoJPADAO();
            Properties pro = new Properties();
            pro.load(TipoGrupoJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
            bd=pro.getProperty("bdJDBC");
            login=pro.getProperty("loginJDBC");
            password=pro.getProperty("passwordJDBC");
            url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                           .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);


            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());


            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
            .getContextClassLoader()
            .getResourceAsStream("test-DAO-dataset.xml"));

        } catch (InstantiationException ex) {
            System.out.println("Hubo un problema al iniciar la base de datos del test: ");
            ex.printStackTrace();
        } catch (IllegalAccessException iae) {
            System.out.println("Hubo un problema al acceder a la base de datos. del test");
            iae.printStackTrace();
        } catch (SQLException sqle) {
            System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
            sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            System.out.println("No se encuentra el driver JDBC."+cnfe);
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error al accerder al archivo de configuración: "+ioe);
            ioe.printStackTrace();   
        }catch (Exception ex) {
                System.out.println("Error inesperado al iniciar los test");
                ex.printStackTrace();
        }
    }
        @AfterClass
    public static  void finalizar() throws Exception{
            if(connection!=null){
                    connection.close();
            }
    }

    @Before
    public void setUp() throws Exception{
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testNuevoTipoGrupo() 
    {
        try
        {
            TipoGrupo o1=new TipoGrupo();
            o1.setMaximoComponentes(1);
            o1.setDescripcion("Drama");

            assert(o1.getMaximoComponentes()==1);
            assert(o1.getDescripcion().equals("Drama"));

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se han insertado el tipo de Obra al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
    
     @Test
    public void testObtenerTodosTipoGrupo() 
    {
        try
        {
            List<TipoGrupo> tipoGrupos= idao.obtenertodosTipoGrupo();
            assert(tipoGrupos.size()==5);

            TipoGrupo a2=null;
            
            for(TipoGrupo to :tipoGrupos)
                    if(to.getMaximoComponentes()==1)
                            a2=to;

            assertNotNull(a2);
            assert(a2.getMaximoComponentes()==1);
            assert(a2.getDescripcion().equals("Drama"));

        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de jugadores al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testObtenerTipoGrupo() 
    {
        try
        {
            TipoGrupo g=idao.obtenerTipoGrupo("Drama");
            assert(g.getMaximoComponentes()==1);
            assert(g.getDescripcion().equals("Drama"));
            
        }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido obtener la lista de equipos al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
    
     @Test
    public void testEliminarTipoGrupo() 
    {
        try
        {
            idao.eliminarTipoGrupo("Drama");

            List<TipoGrupo> grupos=null;
            grupos=idao.obtenertodosTipoGrupo();

            if(grupos.size()!=4)
                fail("TEST NO SUPERADO: No se ha eliminado el tipo de obra");

            if(idao.obtenerTipoGrupo("Drama")!=null)
                fail("TEST NO SUPERADO: No se ha eliminado el tipo de obra Seleccionado");

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido obtener la lista de tipos de obra al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
    
     @Test
    public void testModificaTipoGrupo() 
    {
        try
        {
            idao.modificaTipoGrupo(5,"Ciencia Ficcion");
  
            TipoGrupo to=idao.obtenerTipoGrupo("Ciencia Ficcion");
            assert(to.getDescripcion().equals("Ciencia Ficcion"));

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido modificar el tipo de obra al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
    
}
