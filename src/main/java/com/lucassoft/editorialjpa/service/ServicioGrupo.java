/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;


import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.GrupoJPADAO;
import com.lucassoft.editorialjpa.entidades.Grupo;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kraftwerk
 */
public class ServicioGrupo {
    
    GrupoJPADAO idao=null;
    ServicioEditorial se=null;
    ServicioTipoObra sto=null;
    
    public ServicioGrupo() throws DAOException
	{
		idao=new GrupoJPADAO();
               
	}
    
    public void nuevoGrupo(String email, String password, String nombre, String pais, String provincia, String localidad, String codigoPostal, String direccion, String nombreResponsable, String cargoResponsable) throws ServicioException, DAOException
	{
            Grupo e = new Grupo();
		
           List<Grupo> grupos = idao.obtenerTodosGrupos();
        if(grupos.isEmpty())
            e.setId(0);
        else
        {
            long id= grupos.get(grupos.size()-1).getId()+1;
            e.setId(id);
        } 
            e.setEmail(email);
            e.setPassword(password);
            e.setNombre(nombre);
            e.setPais(pais);
            e.setProvincia(provincia);
            e.setLocalidad(localidad);
            e.setCodigopostal(codigoPostal);
            e.setDireccion(direccion);
            e.setNombreResponsable(nombreResponsable);
            e.setCargoResponsable(cargoResponsable);
             
            Grupo ePrueba=idao.obtenerGrupoEmail(email);
        if(ePrueba!=null)
            throw new ServicioException("Ya existe un grupo con ese email");
        
        Grupo ePruebaEmail=idao.obtenerGrupoEmail(e.getEmail());
        if(ePruebaEmail!=null)
        	throw new ServicioException("Ya existe un Grupo con ese email");
        
        idao.nuevoGrupo(e);
	}
    
      public Grupo obtenerGrupo(String email) throws DAOException
	{
            Grupo g = idao.obtenerGrupoEmail(email);

           return g;
        }
      
      public List<Grupo> obtenerTodasGrupos() throws DAOException
	{
		 List<Grupo> grupo= idao.obtenerTodosGrupos();
	        
	        if(grupo==null)
	        	grupo=new ArrayList<Grupo>();
	        
	        return grupo;
	}
      
      public void borraGrupo(String email)throws DAOException
	{
            
        try 
        {
            Grupo grupo = idao.obtenerGrupoEmail(email);

            if (grupo==null)
                throw new ServicioException("El grupo no existe.");

           int resultado = idao.eliminarGrupo(email);

            if (resultado!=1)
                throw new ServicioException("No se ha eliminado el grupo con email: "+ email);

        } catch (ServicioException ex) {
            Logger.getLogger(ServicioTipoObra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
      public void modificaGrupo(String email, String password, String nombre, String pais, String provincia, String localidad, String codigoPostal, String direccion, String nombreResponsable, String cargoResponsable) throws DAOException, ServicioException
	{
		Grupo e = new Grupo();
		
		e.setEmail(email);
                e.setPassword(password);
                e.setNombre(nombre);
                e.setPais(pais);
                e.setProvincia(provincia);
                e.setLocalidad(localidad);
                e.setCodigopostal(codigoPostal);
                e.setDireccion(direccion);
                e.setNombreResponsable(nombreResponsable);
                e.setCargoResponsable(cargoResponsable);
                
            Grupo ePrueba=idao.obtenerGrupoEmail(email);
                    
            if(ePrueba==null)
                throw new ServicioException("No existe un grupo con ese email");
            
            int afectadas = idao.modificaGrupo(email,e);
        
            if (afectadas==0)
        	throw new ServicioException("No es ha modificado ningun grupo.");
        
	}
    
}
