package com.lucassoft.editorialjpa.entidades;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Version;


@Entity
@DiscriminatorValue("Cliente")

public abstract class Cliente extends Usuario implements Serializable{

    @Version
    private static final long serialVersionUID = 1L;
    
        protected String pais;
        protected String provincia;
        protected String localidad;
        protected String codigopostal;
        protected String direccion;

    
        public void setPais(String pais) {
        this.pais = pais;
        }
	public String getPais(){
        return pais;}
        
        public String getProvincia() {
        return provincia;
        }   
        public void setProvincia(String provincia) {
        this.provincia = provincia;
        }
        
        public String getLocalidad() {
        return localidad;
        }
        public void setLocalidad(String localidad) {
        this.localidad = localidad;
        }
        
        public String getCodigopostal() {
        return codigopostal;
        }
        public void setCodigopostal(String codigopostal) {
        this.codigopostal = codigopostal;
        }        
        
        public String getDireccion() {
        return direccion;
        }	
        public void setDireccion(String direccion) {
        this.direccion = direccion;
        }
}
