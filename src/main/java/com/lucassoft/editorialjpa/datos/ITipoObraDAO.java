package com.lucassoft.editorialjpa.datos;

import java.util.List;

import com.lucassoft.editorialjpa.entidades.TipoObra;


public interface ITipoObraDAO 
{
	 public void nuevoTipoObra(TipoObra t) throws DAOException;
         public int eliminarTipoObra(String nombre) throws DAOException;
	 public List<TipoObra> obtenertodosTipoObra() throws DAOException;
	 public TipoObra obtenerTipoObra(String nombre) throws DAOException;
         public int modificaTipoObra(String nombre, String descrip) throws DAOException;
}
