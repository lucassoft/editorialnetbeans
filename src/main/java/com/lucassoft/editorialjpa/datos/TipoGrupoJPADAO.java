/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.TipoGrupo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Abraham
 */
public class TipoGrupoJPADAO implements ITipoGrupo {

      EntityManagerFactory emf=null;
    
    public TipoGrupoJPADAO() throws DAOException 
    {
        try
        {
            emf= Emf.getInstance().getEmf();
        
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        }
    }
    public void nuevoTipoGrupo(TipoGrupo tg) throws DAOException {
        
         EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try 
        {
            em.persist(tg);
            em.getTransaction().commit();
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al guardar el tipo de grupo", ex);  
        } finally {
            em.close(); 
        }
    }

    public int eliminarTipoGrupo(String descripcion) throws DAOException {
        
          EntityManager em = emf.createEntityManager();
        try
        {
            TipoGrupo identTO=this.obtenerTipoGrupo(descripcion);
            long id = identTO.getId();
         
            em.getTransaction().begin();
                TipoGrupo to = em.find(TipoGrupo.class,id);
            em.remove(to);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el tipo grupo", ex);     
        } finally {
            em.close(); 
        }        
    }

    public List<TipoGrupo> obtenertodosTipoGrupo() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public TipoGrupo obtenerTipoGrupo(String descripcion) throws DAOException {
          
        EntityManager em = emf.createEntityManager();
        
        try
        {
            TipoGrupo e = null;
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT e FROM TipoGrupo e WHERE e.descripcion=:descripcion");
            query.setParameter("descripcion",descripcion);
            List tipoGrupoRes=query.getResultList();
            List<TipoGrupo> edit=(List<TipoGrupo>) tipoGrupoRes;

            if(edit.size()==0)
                return null;

            return edit.get(0);
             
        } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al obtener el tipo de grupo", ex);
        } finally {
            em.close();   
        }
    }

    public int modificaTipoGrupo(int maximoComponentes, String descripcion) throws DAOException {
         
         EntityManager em = emf.createEntityManager();
        try
        {

            TipoGrupo identTO=this.obtenerTipoGrupo(descripcion);
            long id = identTO.getId();
            
            em.getTransaction().begin();
            em.flush();
            
                TipoGrupo to = em.find(TipoGrupo.class,id);
 
            to.setDescripcion(descripcion);
            to.setMaximoComponentes(maximoComponentes);
            
            em.persist(to);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el tipo de grupo", ex);      
        } finally {
            em.close(); 
        }
    }
       public TipoGrupo obtenerTipoGrupo(long id) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            TipoGrupo j=em.find(TipoGrupo.class,id);

             return j;
             
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el tipo de grupo", ex);
        } finally {
            em.close(); 
        }
    } 
       
        public void finalizar() throws DAOException 
    {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }
        
    
}
