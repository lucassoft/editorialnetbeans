package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Coleccion;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class ColeccionJPADAO implements IColeccionDAO{
    
    EntityManagerFactory emf=null;
    
    public ColeccionJPADAO() throws DAOException 
    {
        try
        {
            emf= Emf.getInstance().getEmf();
            
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        } 
    }
    
     public void nuevaColeccion(Coleccion sn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try 
        {
            em.persist(sn);
            em.getTransaction().commit();
            
        } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar la coleccion", ex);
        } finally {
            em.close();  
        } 
    }
     
     
     public Coleccion obtenerColeccion(String isbn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            Coleccion c=null;
          
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();                
            
            Query query = em.createQuery("SELECT c FROM Coleccion c WHERE c.ISBN=:isbn");
            query.setParameter("isbn",isbn);
            List coleccion=query.getResultList();
            List<Coleccion> colecc=(List<Coleccion>) coleccion;
            
            if(colecc.isEmpty())
                return null;
            
            return colecc.get(0);
                      
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener la coleccion", ex);
        } finally {
            em.close(); 
        } 
    }
     
     
      public List<Coleccion> obtenerTodasColecciones() throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT c FROM Coleccion c ");
            
            List coleccion = query.getResultList();
            return coleccion;
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener las colecciones", ex);
        } finally {
            em.close();
        }
    }
      
      
       public void eliminarColeccion(String isbn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try
        {
            Coleccion c= this.obtenerColeccion(isbn);
            long id= c.getId();
            
            Coleccion sn=em.find(Coleccion.class,id);

            em.remove(sn);
            em.getTransaction().commit();
         
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al eliminar la coleccion", ex);
        } finally {
            em.close();
        }
    }
       
       
       public int modificaColeccion(String isbn, Coleccion sn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        try
        {
            em.getTransaction().begin();
            em.flush();

            Coleccion identSN=this.obtenerColeccion(isbn);
            long id = identSN.getId();

            Coleccion coleccion = em.find(Coleccion.class,id);
            
            //coleccion.setISBN(sn.getISBN());
            coleccion.setNombre(sn.getNombre());
            coleccion.setAutor(sn.getAutor());
            coleccion.setIdioma(sn.getIdioma());
            coleccion.setDerechos(sn.getDerechos());
            coleccion.setEditorial(sn.getEditorial());
            coleccion.setTipo(sn.getTipo());
            coleccion.setSubtipoObra(sn.getSubtipoObra());
            coleccion.setDescripcion(sn.getDescripcion());
            coleccion.setObras(sn.getObras());
            

            em.persist(coleccion);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener la coleccion", ex);  
        } finally {
            em.close(); 
        }
    } 
    
       
       public void finalizar() throws DAOException 
    {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }
}
