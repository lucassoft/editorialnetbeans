/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.ClienteIndividual;
import java.util.List;

/**
 *
 * @author user
 */
public interface IClienteIndividualDAO {
    
    public void nuevoClienteIndividual(ClienteIndividual u) throws DAOException;
    public void eliminarClienteIndividual(long id) throws DAOException;
    public ClienteIndividual obtenerClienteIndividualEmail(String email) throws DAOException;
    public List<ClienteIndividual> obtenerTodosClienteIndividual() throws DAOException;
    public void finalizar() throws DAOException;
    public int modificaClienteIndividual(String email, ClienteIndividual e) throws DAOException;
    
}
