package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Administrador;
import com.lucassoft.editorialjpa.entidades.Editorial;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AdministradorJPADAOTEST{
	
	//Fíjate son variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static IAdministradorDAO idao=null;
	
	
	//Este método se ejucuta UNA vez, justo antes de empezar las pruebas
	@BeforeClass
	public static void inicializarBD() throws Exception {
		

        try {
       	 //Ojo esta es una clase de test obtendrá la configuración
        //del .properties de src/TEST/resources NO de src/main/resources
       	 Properties pro = new Properties();
   		 pro.load(AdministradorJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

       	 
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	         idao= new AdministradorJPADAO();
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
	
	//Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
                //Esto es nuevo hay que cerrar la factoria de sesiones
		idao.finalizar();
	}
	
	
	//Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
	
	//Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	
	
	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void testObtenerTodosAdministradores() {
		try{
			List<Administrador> administradores= idao.obtenerTodosAdministradores();
			
			assert(administradores.size()==5);		
			
			Administrador a=administradores.get(0);
			
			assert(a.getEmail().equals("19@email.com"));
			assert(a.getPassword().equals("1"));
			assert(a.getNombre().equals("ed1"));			
			
			Administrador ad = null;
			
			for(Administrador a2 : administradores)
				if(a2.getNombre().equals("ed1"))
					ad=a2;
			
			assertNotNull(ad);
			assert(ad.getNombre().equals("ed1"));
			assertNotNull(ad.getEmail());
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de adminstradores al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	
	
	@Test
	public void testNuevoAdministrador() {
		try{
			
			Administrador a1= new Administrador();
                        List<Administrador>administradores = idao.obtenerTodosAdministradores();
                        long id= administradores.get(administradores.size()-1).getId()+1;
			
                        a1.setId(id); 
			a1.setEmail("prueba@email.com");
			a1.setPassword("prueba");
			a1.setNombre("EditorialKiko");
			
			idao.nuevoAdministrador(a1);
			
			administradores=idao.obtenerTodosAdministradores();
			
			Administrador a2=null;
			for(Administrador a :administradores)
				if(a.getNombre().equals("EditorialKiko"))
					a2=a;
				
			
			assertNotNull(a2);
			assert(a2.getEmail().equals("prueba@email.com"));
			assert(a2.getPassword().equals("prueba"));
			assert(a2.getNombre().equals("EditorialKiko"));
		
			
			
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha insertado El administrador al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}

	
	@Test
	public void testObtenerAdministrador() {
		
		try
		{
			Administrador a=idao.obtenerAdministradorEmail("19@email.com");
			
			if(a.getEmail()==null)
				fail("No se ha obtenido la editorial apropiadamente, la editorial no tiene email");
			
			if(!(a.getPassword().equals("1")))
				fail("No se ha obtenido la editorial apropiadamente, la contraseña no es la correcta");
			
			if(!(a.getNombre().equals("ed1")))
				fail("No se ha obtenido la editorial apropiadamente, el nombre no es el correcto");
			
		
			
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de Administradores al producirse una excepcion: "+e.getLocalizedMessage());
                }
	}
        
        
    @Test
    public void testModificaAdministrador() 
    {
        try
        {
            Administrador a = new Administrador();
            
            a.setPassword("111");
            a.setNombre("edUno");
            a.setEmail("19@email.com");
          
            idao.modificaAdministrador("19@email.com",a);
                
            Administrador ad = idao.obtenerAdministradorEmail("19@email.com");
                
            assert(ad.getPassword().equals("111"));
            assert(ad.getNombre().equals("edUno"));
          
        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido modificar el Administrador al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
	

	@Test
	public void testEliminaAdministrador() 
        {
            try
            {
                List<Administrador> administradores=null;
                idao.eliminarAdministrador("19@email.com");
                administradores=idao.obtenerTodosAdministradores();
                
                if(!(administradores.size()==4))
                    fail("TEST NO SUPERADO: No se ha eliminado la editorial");
                
                if(idao.obtenerAdministradorEmail("2")!=null)
                    fail("TEST NO SUPERADO: No se ha eliminado El administrador seleccionada");
   
            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha podido obtener la lista de administradores al producirse una excepcion: "+e.getLocalizedMessage());
            }
	}
}
