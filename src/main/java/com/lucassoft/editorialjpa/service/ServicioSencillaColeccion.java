package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.EditorialJPADAO;
import com.lucassoft.editorialjpa.datos.SencillaColeccionJPADAO;
import com.lucassoft.editorialjpa.datos.TipoObraJPADAO;
import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.SencillaColeccion;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import java.util.ArrayList;
import java.util.List;

public class ServicioSencillaColeccion 
{

    SencillaColeccionJPADAO idao=null;
    EditorialJPADAO idaoe=null;
    TipoObraJPADAO idaoto=null;
    
	public ServicioSencillaColeccion() throws DAOException
	{
            idao=new SencillaColeccionJPADAO();
            idaoe=new EditorialJPADAO();
            idaoto=new  TipoObraJPADAO();
	}
        
        
        public void nuevaSencillaColeccion(String isbn, String nombre, String autor, String idioma, boolean derechos,String cif, String nombreTipoObra, String subTiObra, String descrip) throws ServicioException, DAOException
	{
            SencillaColeccion o = new SencillaColeccion();
		
            List<SencillaColeccion> sencillaColeccion = idao.obtenerTodasSencillaColeccion();
            if(sencillaColeccion.isEmpty())
                o.setId(0);
            else
            {
                long id= sencillaColeccion.get(sencillaColeccion.size()-1).getId()+1;
                o.setId(id);
            }
        
            o.setISBN(isbn);
            o.setNombre(nombre);
            o.setAutor(autor);
            o.setIdioma(idioma);
            o.setDerechos(derechos);            
            
            Editorial e = idaoe.obtenerEditorial(cif);
            
                if(e==null)
                    throw new ServicioException("No existe ningúna editorial con ese cif");
            
            o.setEditorial(e);
            
            TipoObra to = idaoto.obtenerTipoObra(nombreTipoObra);
            
                if(to==null)
                    throw new ServicioException("No existe ningún tipo de obra con ese nombre");
            
            o.setTipo(to);  
            o.setSubtipoObra(subTiObra);
            o.setDescripcion(descrip);

            SencillaColeccion oPrueba=idao.obtenerSencillaColeccion(isbn);
            
            if(oPrueba!=null)
                throw new ServicioException("Ya existe una sencilla coleccion con ese ISBN");
        
            idao.nuevaSencillaColeccion(o);
	}
        
        
        public SencillaColeccion obtenerSencillaColeccion(String isbn) throws DAOException, ServicioException
	{ 
            SencillaColeccion o= idao.obtenerSencillaColeccion(isbn);
	        
	    if(o==null)
                    throw new ServicioException("No existe ningúna obra con ese isbn");    
	        
	    return o;
	}
        
        public List<SencillaColeccion> obtenerTodasSencillaColeccion() throws DAOException
	{
            List<SencillaColeccion> sencillas= idao.obtenerTodasSencillaColeccion();

            if(sencillas==null)
                sencillas=new ArrayList<SencillaColeccion>();

            return sencillas;
	}
        
        public void eliminarSencillaColeccion(String isbn) throws ServicioException, DAOException
	{
            SencillaColeccion e = idao.obtenerSencillaColeccion(isbn);
            if(e==null)
                throw new ServicioException("La editorial que quieres borrar no existe.");
            else
            idao.eliminarSencillaColeccion(isbn);
	}
        
        
        
        public void modificaSencillaColeccion(String isbn, String nombre, String autor, String idioma, boolean derechos,String cif, String nombreTipoObra, String subTiObra, String descrip) throws DAOException, ServicioException
	{
            SencillaColeccion o = new SencillaColeccion();
		
            o.setISBN(isbn);
            o.setNombre(nombre);
            o.setAutor(autor);
            o.setIdioma(idioma);
            o.setDerechos(derechos);
            
            Editorial e = idaoe.obtenerEditorial(cif);
            
                if(e==null)
                    throw new ServicioException("No existe ningúna editorial con ese cif");
            
            o.setEditorial(e);
            
            TipoObra to = idaoto.obtenerTipoObra(nombreTipoObra);
            
                if(to==null)
                    throw new ServicioException("No existe ningún tipo de obra con ese nombre");
            
            o.setTipo(to);    
            o.setSubtipoObra(subTiObra);
            o.setDescripcion(descrip);
                
            SencillaColeccion snPrueba=idao.obtenerSencillaColeccion(isbn);
                    
            if(snPrueba==null)
                throw new ServicioException("No existe una obra con ese isbn");
            
            int afectadas = idao.modificaSencillaColeccion(isbn, o);
        
            if (afectadas==0)
        	throw new ServicioException("No es ha modificado ninguna obra.");
	}
}
