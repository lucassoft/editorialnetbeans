/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.TipoGrupoJPADAO;
import com.lucassoft.editorialjpa.entidades.TipoGrupo;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
 

/**
 *
 * @author Abraham
 */
public class ServicioTipoGrupo {
    
     TipoGrupoJPADAO idao=null;

    public ServicioTipoGrupo() throws DAOException
    {
            idao=new TipoGrupoJPADAO();
    }
    
      public void nuevoTipoGrupo(int maximoComponentes, String descripcion) throws ServicioException, DAOException
    {
        TipoGrupo t = new TipoGrupo();

        t.setMaximoComponentes(maximoComponentes);
        t.setDescripcion(descripcion);

        TipoGrupo ePrueba=idao.obtenerTipoGrupo(t.getDescripcion());//falta la query del update
            
        if(ePrueba!=null)
            throw new ServicioException("Ya existe un TipoGrupo con esa descripcion");

        idao.nuevoTipoGrupo(t);
    }
      
      public TipoGrupo obtenerTipoGrupo(String descripcion) throws DAOException
    { 
        TipoGrupo t= idao.obtenerTipoGrupo(descripcion);

        return t;
    }
         public TipoGrupo obtenerTipoGrupo(int id) throws DAOException
    { 
        TipoGrupo t= idao.obtenerTipoGrupo(id);

        return t;
    }
      
      public List<TipoGrupo> obtenerTodosTipoGrupo() throws DAOException
    {
        List<TipoGrupo> tipogrupo= idao.obtenertodosTipoGrupo();

        if(tipogrupo==null)
            tipogrupo=new ArrayList<TipoGrupo>();

        return tipogrupo;
    }
      
      
      public void eliminarTipoGrupo(String descripcion) throws  DAOException
    {	
        try 
        {
            TipoGrupo tipogrupo = idao.obtenerTipoGrupo(descripcion);

            if (tipogrupo==null)
                throw new ServicioException("El tipo de grupo no existe.");

            int resultado = idao.eliminarTipoGrupo(descripcion);

            if (resultado!=1)
                throw new ServicioException("No se ha eliminado el tipo grupo "+ descripcion);

        } catch (ServicioException ex) {
            Logger.getLogger(ServicioTipoObra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      
       public void modificaTipoGrupo(int maximoComponentes, String descripcion) throws DAOException, ServicioException
    { 	   
        int afectadas = idao.modificaTipoGrupo(maximoComponentes, descripcion);

        if (afectadas == 0)
            throw new ServicioException("No es ha modificado ningun tipogrupo.");
    }
    
    
}
