/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.AdministradorJPADAO;
import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.entidades.Administrador;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kiko
 */
public class ServicioAdministrador {
    AdministradorJPADAO idao=null;
    
    public ServicioAdministrador() throws DAOException
	{
		idao=new AdministradorJPADAO();
	}
    
    public void nuevoAdministrador(String email, String password, String nombre) throws ServicioException, DAOException
	{
		Administrador a = new Administrador();
                 List<Administrador>administradores = idao.obtenerTodosAdministradores();
                    if(administradores.isEmpty())
                        {
                            a.setId(0);
                        }
                    else
                        {
                        long id= administradores.get(administradores.size()-1).getId()+1;
                        a.setId(id);
                        }
		                
		a.setEmail(email);
		a.setPassword(password);
		a.setNombre(nombre);
               
		Administrador aPrueba=idao.obtenerAdministradorEmail(email);
        if(aPrueba!=null)
            throw new ServicioException("Ya existe un cliente individual con ese email");
        
        Administrador aPruebaEmail=idao.obtenerAdministradorEmail(a.getEmail());
        if(aPruebaEmail!=null)
        	throw new ServicioException("Ya existe una clienteindividual con ese email");
        
        idao.nuevoAdministrador(a);
	}
    
    
    public Administrador obtenerAdministrador(String email) throws DAOException
	{
            Administrador a = idao.obtenerAdministradorEmail(email);

           return a;
        }
    
    
    public List<Administrador> obtieneAdministradores() throws DAOException
	{
		 List<Administrador> Administradores= idao.obtenerTodosAdministradores();
	        
	        if(Administradores==null)
	        	Administradores=new ArrayList<Administrador>();
	        
	        return Administradores;
	}
    
    
    public void borraAdministrador(String email) throws DAOException
	{
            try 
            {
                Administrador a = idao.obtenerAdministradorEmail(email);

                if (a==null)
                    throw new ServicioException("El tipo de obra no existe.");

                int resultado = idao.eliminarAdministrador(email);

                if (resultado!=1)
                    throw new ServicioException("No se ha eliminado el Administrador con"+ email);

            } catch (ServicioException ex) {
                Logger.getLogger(ServicioAdministrador.class.getName()).log(Level.SEVERE, null, ex);
            }
	}
    
    
    public void modificaAdministrador(String email, String password, String nombre) throws DAOException, ServicioException
	{
		Administrador a = new Administrador();
		
		a.setEmail(email);
		a.setPassword(password);
		a.setNombre(nombre);
		
                
            Administrador aPrueba=idao.obtenerAdministradorEmail(email);
                    
            if(aPrueba==null)
                throw new ServicioException("No existe un Administrador con ese email");
            
            int afectadas = idao.modificaAdministrador(email,a);
        
            if (afectadas==0)
        	throw new ServicioException("No es ha modificado ningun Administrador.");
        
	}
}
