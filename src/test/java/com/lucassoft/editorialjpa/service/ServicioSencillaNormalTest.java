package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.SencillaColeccionJPADAOTEST;
import com.lucassoft.editorialjpa.datos.SencillaNormalJPADAOTEST;
import com.lucassoft.editorialjpa.entidades.SencillaColeccion;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ServicioSencillaNormalTest {
    
        private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static ServicioSencillaNormal sf=null;
	
	@BeforeClass
	public static void inicializarBD() throws Exception {

        try {

         sf= new ServicioSencillaNormal();
       	 Properties pro = new Properties();
   		 pro.load(SencillaNormalJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            sf= new ServicioSencillaNormal();
            
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
                 
                  sf= new ServicioSencillaNormal();
	      //sf.elegirSistemaAlmacenamiento(5);
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
         //Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
	}
        
        //Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
        
        //Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	@After
	public void tearDown() throws Exception {
	}
	
          @Test
	public void testNuevoSencillaNormal()
        {	
            try
            {             
                
              sf.nuevaSencillaNormal("450-55","1984", "Geroge Orwell", "Español", true, "2","tipoobra1","Politica","prueba",5);
 
                List<SencillaNormal> sencillaNormal = sf.obtenerTodasSencillas();
                if (sencillaNormal.size() != 6) 
                        fail("No se ha insertado la Sencilla Coleccion");

                for (SencillaNormal t : sencillaNormal) 
                {
                    assertNotNull(t.getISBN());
                    assertNotNull(t.getNombre());
                    assertNotNull(t.getAutor());
                    assertNotNull(t.getIdioma());
                    assertNotNull(t.getDerechos());
                    assertNotNull(t.getSubtipoObra());
                    assertNotNull(t.getDescripcion());
                    //assertNotNull(t.getArchivo());
                    assertNotNull(t.getPrecioIndividual());
                    assertNotNull(t.getEditorial());
                    assertNotNull(t.getTipo());
                    
                }

            } catch (Exception e) {
                    fail("TEST NO SUPERADO: No se ha insertado la Sencilla Coleccion al producirse una excepcion: " + e.getLocalizedMessage());
            }			
        }
        @Test(expected = ServicioException.class)
		public void testNuevoSencillaNormalConISBNRepetidoFail() throws Exception {
			
			
			sf.nuevaSencillaNormal("450-55","1984", "Geroge Orwell", "Español", true,"prueba","Politica","2" ,"tipoobra1",5);

		}
                
                 @Test
                public void testModificarSencillaNormal(){
                    try
                    {
                        sf.modificaSencillaNormal("442-55","1920", "Geroge Orwell55", "chino", true,"2","tipoobra1","Politica","prueba",5);
            
                    } catch (Exception e) {
			fail("TEST NO SUPERADO: No se ha modificado la Sencilla Coleccion al producirse una excepcion: "
					+ e.getLocalizedMessage());
            }
	}
                
                @Test
		public void testObtenerTodosSencillaNormal()
                {	
                    try 
                    {

                        List<SencillaNormal> sencillaN = sf.obtenerTodasSencillas();
                        if (sencillaN.size() != 5) 
                                fail("No se han obtenido todas la Sencilla Coleccion");

                        for (SencillaNormal sc : sencillaN) 
                        {
                            assertNotNull(sc.getId());
                            assertNotNull(sc.getISBN());
                            assertNotNull(sc.getNombre());
                            assertNotNull(sc.getAutor());
                            assertNotNull(sc.getIdioma());
                            assertNotNull(sc.getDerechos());
                            assertNotNull(sc.getSubtipoObra());
                            assertNotNull(sc.getDescripcion());
                            assertNotNull(sc.getArchivo());
                            assertNotNull(sc.getPrecioIndividual());
                            assertNotNull(sc.getEditorial());
                            assertNotNull(sc.getTipo());
                        }

                    } catch (Exception e) {
                            fail("TEST NO SUPERADO: No se han obtenido la Sencilla Coleccion al producirse una excepcion: " + e.getLocalizedMessage());
                    }
		}
                
                 @Test   
		public void testEliminarSencillaNormal()
                {	
                    try 
                    {
                        sf.eliminaSencillaNormal("442-55");
                        List<SencillaNormal> sencillanormal = sf.obtenerTodasSencillas();
                        
                        if (sencillanormal.size() != 4) 
                                fail("No se ha eliminado la Sencilla Coleccion");
                        
                        for (SencillaNormal sc : sencillanormal) 
                        {
                             assertNotNull(sc.getId());
                            assertNotNull(sc.getISBN());
                            assertNotNull(sc.getNombre());
                            assertNotNull(sc.getAutor());
                            assertNotNull(sc.getIdioma());
                            assertNotNull(sc.getDerechos());
                            assertNotNull(sc.getSubtipoObra());
                            assertNotNull(sc.getDescripcion());
                            assertNotNull(sc.getArchivo());
                            assertNotNull(sc.getPrecioIndividual());
                            assertNotNull(sc.getEditorial());
                            assertNotNull(sc.getTipo());
                        }

                    } catch (Exception e) {
                            fail("TEST NO SUPERADO: No se ha eliminado la Sencilla Coleccion al producirse una excepcion: "
                                            + e.getLocalizedMessage());
                    }
		}  
                
    
}
