/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;


/**
 *
 * @author Saul
 */
public class EditorialJPADAO implements IEditorialDAO{
    EntityManagerFactory emf=null;

    public EditorialJPADAO() throws DAOException {
        try{
            //Creamos la factoria
            emf= Emf.getInstance().getEmf();
        
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        }
        
    }
    
    public void nuevaEditorial(Editorial e) throws DAOException {
        

        // Creamos el entity Manager
         
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try {
            
            em.persist(e);
            em.getTransaction().commit();
            
        } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar la editorial", ex);
            
            
        } finally {
            em.close();
            
        }
    }
    
     public int eliminarEditorial(String cif) throws DAOException 
     {
        EntityManager em = emf.createEntityManager();
        try
        {
            Editorial identE=this.obtenerEditorial(cif);
            long id = identE.getId();
         
            em.getTransaction().begin();
                Editorial e = em.find(Editorial.class,id);
            em.remove(e);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener la editorial", ex);     
        } finally {
            em.close(); 
        }      
    }
     
     
     public Editorial obtenerEditorial(String cif) throws DAOException {
        EntityManager em = emf.createEntityManager();
         try{
                Editorial e = null;
                em.getTransaction().begin();
                em.flush();
                em.getTransaction().commit();
                
                Query query = em.createQuery("SELECT e FROM Editorial e WHERE e.cif=:cif");
                query.setParameter("cif",cif);
                List editorial=query.getResultList();
                List<Editorial> edit=(List<Editorial>) editorial;
                if(edit.size()==0){
                    return null;
                }
                return edit.get(0);
                    
                
                
                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener la editorial", ex);
                
        } finally {
            em.close(); 
        }
    }
     
     public List<Editorial> obtenerTodasEditoriales() throws DAOException {
      EntityManager em = emf.createEntityManager();
      try{
      em.getTransaction().begin();
      em.flush();
      em.getTransaction().commit();
      //Para realizar consultas usaremos el lenguaje JPQL, muy parecido a HQL y a SQL
      //Al igual que HQL recuerda que no hacemos consultas sobre tablas sino sobre objetos
      Query query = em.createQuery("SELECT e FROM Editorial e ");
      //En caso de necesitar parámetros los ontroducimos así en la consulta sería con : igual que HQL
      //query.setParameter("parametro",valor);
      List ed = query.getResultList();
      return ed;
      } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al obtener las editoriales", ex);
        } finally {
            em.close();
            
        }
    }
     
     public void finalizar() throws DAOException {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }


    public Editorial obtenerEditorialEmail(String email) throws DAOException {
         EntityManager em = emf.createEntityManager();
         try{
                Editorial e = null;
                em.getTransaction().begin();
                em.flush();
                em.getTransaction().commit();
                
                Query query = em.createQuery("SELECT e FROM Editorial e WHERE e.email=:email");
                query.setParameter("email",email);
                List editorial=query.getResultList();
                List<Editorial> edit=(List<Editorial>) editorial;
                if(edit.size()==0){
                    return null;
                }
                return edit.get(0);
                    
                
                
                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener la editorial", ex);
                
        } finally {
            em.close(); 
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int modificaEditorial(String cif, Editorial e) throws DAOException {
         EntityManager em = emf.createEntityManager();
         try{
                
                
                Editorial identEdi=this.obtenerEditorial(cif);
                long id = identEdi.getId();
                
                em.getTransaction().begin();
                em.flush();
                
                    Editorial edi = em.find(Editorial.class,id);

                    edi.setNombre(e.getNombre());
                    edi.setPassword(e.getPassword());
                    edi.setCuentaCorriente(e.getCuentaCorriente());
                    edi.setPais(e.getPais());
                    edi.setRazonSocial(e.getRazonSocial());
                    edi.setTelefono1(e.getTelefono1());
                    edi.setTelefono2(e.getTelefono2());
                
                em.persist(edi);
                em.getTransaction().commit();
                
                return 1;
                                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener la editorial", ex);
                
        } finally {
            em.close(); 
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
