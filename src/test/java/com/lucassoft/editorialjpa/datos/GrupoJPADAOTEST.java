/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.Grupo;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Kraftwerk
 */
public class GrupoJPADAOTEST {
    
    //Fíjate son variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static IGrupoDAO idao=null;
        
        
        //Este método se ejucuta UNA vez, justo antes de empezar las pruebas
	@BeforeClass
	public static void inicializarBD() throws Exception {
		

        try {
       	 //Ojo esta es una clase de test obtendrá la configuración
        //del .properties de src/TEST/resources NO de src/main/resources
       	 Properties pro = new Properties();
   		 pro.load(GrupoJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

       	 
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	         idao= new GrupoJPADAO();
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
    //Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
                //Esto es nuevo hay que cerrar la factoria de sesiones
		idao.finalizar();
	}
	
	
	//Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
        
        @Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
	
	//Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testObtenerTodosGrupos() {
		try{
			List<Grupo> grupos= idao.obtenerTodosGrupos();
			
			assert(grupos.size()==5);
			
			for(Grupo e :grupos)
				assertNotNull(e.getEmail());
			
			Grupo e=grupos.get(0);
			
			assert(e.getEmail().equals("11@email.com"));
			assert(e.getPassword().equals("1"));
			assert(e.getNombre().equals("ed1"));
			assert(e.getPais().equals("pais"));
			assert(e.getProvincia().equals("p1"));
			assert(e.getLocalidad().equals("l1"));
			assert(e.getCodigopostal().equals("1"));
			assert(e.getDireccion().equals("d1"));
			assert(e.getNombreResponsable().equals("r1"));
                        assert(e.getCargoResponsable().equals("c1"));
			
			Grupo edi = null;
			
			for(Grupo e2 : grupos)
				if(e2.getEmail().equals("11@email.com"))
					edi=e;
			
			assertNotNull(edi);
			assert(edi.getNombre().equals("ed1"));
			assertNotNull(edi.getEmail());
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de grupos al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
        
        
        @Test
	public void testNuevoGrupo() {
		try{
			
		Grupo e1= new Grupo();
			
		e1.setEmail("prueba@email.com");
		e1.setPassword("prueba");
		e1.setNombre("EditorialKiko");
		e1.setPais("11111111L");
	        e1.setProvincia("kiko");
	        e1.setLocalidad("619154672");
	        e1.setCodigopostal("965479079");
	        e1.setDireccion("12345678901234567890");
	        e1.setNombreResponsable("España");
                e1.setCargoResponsable("España");
			
			idao.nuevoGrupo(e1);
			
			List<Grupo> grupo=idao.obtenerTodosGrupos();
			
			Grupo e2=null;
			for(Grupo e :grupo)
				if(e.getEmail().equals("prueba@email.com"))
					e2=e;
				
			
			assertNotNull(e2);
			assert(e2.getEmail().equals("prueba@email.com"));
			assert(e2.getPassword().equals("prueba"));
			assert(e2.getNombre().equals("EditorialKiko"));
			assert(e2.getPais().equals("11111111L"));
			assert(e2.getProvincia().equals("kiko"));
			assert(e2.getLocalidad().equals("619154672"));
			assert(e2.getCodigopostal().equals("965479079"));
			assert(e2.getDireccion().equals("12345678901234567890"));
			assert(e2.getNombreResponsable().equals("España"));
                        assert(e2.getCargoResponsable().equals("España"));
			
			
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha insertado el grupo al producirse una excepcion: "+e.getLocalizedMessage());
		}
	}

	@Test
	public void testObtenerGrupo() {
		
		try
		{
			Grupo e=idao.obtenerGrupoEmail("11@email.com");
			
			if(e.getEmail()==null)
				fail("No se ha obtenido el grupo apropiadamente, la editorial no tiene email");
			
			if(!(e.getPassword().equals("1")))
				fail("No se ha obtenido el grupo apropiadamente, la contraseña no es la correcta");
			
			if(!(e.getNombre().equals("ed1")))
				fail("No se ha obtenido el grupo apropiadamente, el nombre no es el correcto");
			
			if(e.getPais()==null)
				fail("No se ha obtenido el grupo apropiadamente, el pais no tiene cif");
			
			if(!(e.getProvincia().equals("p1")))
				fail("No se ha obtenido el grupo apropiadamente, la provincia no tiene razón social");
			
			if(!(e.getLocalidad().equals("l1")))
				fail("No se ha obtenido el grupo apropiadamente, la localidad no es el correcto");
			
			if(!(e.getCodigopostal().equals("1")))
				fail("No se ha obtenido el grupo apropiadamente, el codigo postal no es el correcto");
			
			if(!(e.getDireccion().equals("d1")))
				fail("No se ha obtenido el grupo apropiadamente, la direccion corriente no es la correcta");
			
			if(!(e.getNombreResponsable().equals("r1")))
				fail("No se ha obtenido el grupo apropiadamente, el nombre responsable no es el correcto");
			
                        if(!(e.getCargoResponsable().equals("c1")))
				fail("No se ha obtenido el grupo apropiadamente, el cargo responsable no es el correcto");
			
			
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de editoriales al producirse una excepcion: "+e.getLocalizedMessage());
                }
	}
        
         @Test
    public void testModificaGrupo() 
    {
        try
        {
            Grupo e1 = new Grupo();
            
                e1.setEmail("12@email.com");
		e1.setPassword("prueba");
		e1.setNombre("EditorialKiko");
		e1.setPais("1L");
	        e1.setProvincia("kiko");
	        e1.setLocalidad("61915");
	        e1.setCodigopostal("96");
	        e1.setDireccion("1234");
	        e1.setNombreResponsable("España");
                e1.setCargoResponsable("España");
            
            idao.modificaGrupo("12@email.com",e1);
                
            Grupo edi = idao.obtenerGrupoEmail("12@email.com");
                
           assertNotNull(edi);
            assert(edi.getEmail().equals("12@email.com"));
            assert(edi.getPassword().equals("prueba"));
            assert(edi.getNombre().equals("EditorialKiko"));
            assert(edi.getPais().equals("1L"));
            assert(edi.getProvincia().equals("kiko"));
            assert(edi.getLocalidad().equals("61915"));
            assert(edi.getCodigopostal().equals("96"));
            assert(edi.getDireccion().equals("1234"));
            assert(edi.getNombreResponsable().equals("España"));
            assert(edi.getCargoResponsable().equals("España"));

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido modificar el grupo al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
        
       @Test
	public void testEliminaEditorial() 
        {
            try
            {
                List<Grupo> grupos=null;
                idao.eliminarGrupo("11@email.com");
                grupos=idao.obtenerTodosGrupos();
                
                if(!(grupos.size()==4))
                    fail("TEST NO SUPERADO: No se ha eliminado el grupo");
                
                if(idao.obtenerGrupoEmail("11@email.com")!=null)
                    fail("TEST NO SUPERADO: No se ha eliminado el grupo seleccionada");
   
            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha podido obtener la lista de editoriales al producirse una excepcion: "+e.getLocalizedMessage());
            }
	}
    }
	

	

