package com.lucassoft.editorialjpa.entidades;

import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="ObraType",discriminatorType=DiscriminatorType.STRING)
@DiscriminatorValue("Obra")
public abstract class Obra implements Serializable {
		  @Version
    private static final long serialVersionUID = 1L;
	
                @Id  
		private long id;
		private String ISBN;
		private String nombre;
		private String autor;
		private String idioma;
		private boolean derechos;
                
                @ManyToOne
		private Editorial editorial;
                
                @ManyToOne
		private TipoObra tipo;
                
		private String subtipoObra;
		private String descripcion;
		
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getISBN() {
			return ISBN;
		}
		public void setISBN(String iSBN) {
			ISBN = iSBN;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getAutor() {
			return autor;
		}
		public void setAutor(String autor) {
			this.autor = autor;
		}
		public String getIdioma() {
			return idioma;
		}
		public void setIdioma(String idioma) {
			this.idioma = idioma;
		}
		public boolean getDerechos() {
			return derechos;
		}
		public void setDerechos(boolean derechos) {
			this.derechos = derechos;
		}
		public Editorial getEditorial() {
			return editorial;
		}
		public void setEditorial(Editorial editorial) {
			this.editorial = editorial;
		}
		public TipoObra getTipo() {
			return tipo;
		}
		public void setTipo(TipoObra tipo) {
			this.tipo = tipo;
		}
		public String getSubtipoObra() {
			return subtipoObra;
		}
		public void setSubtipoObra(String subtipoObra) {
			this.subtipoObra = subtipoObra;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		

	}



