package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Administrador;
import java.util.List;

public interface IAdministradorDAO 
{
	public void nuevoAdministrador(Administrador a) throws DAOException;
        public void finalizar() throws DAOException;
	public Administrador obtenerAdministradorEmail(String email) throws DAOException;
	public int eliminarAdministrador(String email) throws DAOException;
	public List<Administrador> obtenerTodosAdministradores() throws DAOException;
	public int modificaAdministrador(String email, Administrador a) throws DAOException;
}
