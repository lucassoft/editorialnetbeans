/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.entidades;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

/**
 *
 * @author Abraham
 */

@Entity

public class TipoGrupo implements Serializable {
    
    @Version
    private static final long serialVersionUID = 1L;
    
    @Id 
        private long id;
	private int maximoComponentes;
	private String descripcion;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getMaximoComponentes() {
        return maximoComponentes;
    }

    public void setMaximoComponentes(int maximoComponentes) {
        this.maximoComponentes = maximoComponentes;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
