/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Coleccion;
import java.util.List;

/**
 *
 * @author user
 */
public interface IColeccionDAO {
    
    public void nuevaColeccion(Coleccion col) throws DAOException;
    public Coleccion obtenerColeccion(String ISBN) throws DAOException;
    public List<Coleccion> obtenerTodasColecciones() throws DAOException;
    public void eliminarColeccion(String ISBN) throws DAOException; 
    public int modificaColeccion(String ISBN, Coleccion col) throws DAOException;
    public void finalizar() throws DAOException;
    
    
}
