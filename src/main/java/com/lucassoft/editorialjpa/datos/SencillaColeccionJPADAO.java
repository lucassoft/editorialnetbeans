package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.SencillaColeccion;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author kiko
 */
public class SencillaColeccionJPADAO implements ISencillaColeccionDAO{
    
     EntityManagerFactory emf=null;
     EditorialJPADAO e_idao = null;

    public SencillaColeccionJPADAO() throws DAOException {
        try{
            //Creamos la factoria
        emf= Emf.getInstance().getEmf();
        e_idao = new EditorialJPADAO();
        
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        }
    }

    public void nuevaSencillaColeccion(SencillaColeccion s) throws DAOException {   

        // Creamos el entity Manager         
        EntityManager em = emf.createEntityManager();

        // Marcamos el comienzo de la transacción
        em.getTransaction().begin();
        try {
            // Lo añadimos a la BD
            em.persist(s);
           
            //Hacemos commit es decir guardamos los cambios
            em.getTransaction().commit();
            
        } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar la Obra SencillaColeccion", ex);
        } finally {
            em.close();
            
        }    
    
    }

    public SencillaColeccion obtenerSencillaColeccion(String ISBN) throws DAOException {
        
    EntityManager em = emf.createEntityManager();
         try{
                SencillaNormal s = null;
                em.getTransaction().begin();
                em.flush();
                em.getTransaction().commit();
                
                Query query = em.createQuery("SELECT s FROM SencillaColeccion s WHERE s.ISBN=:ISBN");
                query.setParameter("ISBN",ISBN);
                List sn=query.getResultList();
                List<SencillaColeccion> SencillaColecciones=(List<SencillaColeccion>) sn;
                if(SencillaColecciones.isEmpty()){
                    return null;
                }
                return SencillaColecciones.get(0);
                    
                
                
                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener la Obra SencillaColeccion", ex);
                
        } finally {
            em.close(); 
        }
    
    }

    public void eliminarSencillaColeccion(String ISBN) throws DAOException {
    
         EntityManager em = emf.createEntityManager();
        try
        {
            SencillaColeccion identS=this.obtenerSencillaColeccion(ISBN);
            long id = identS.getId();
         
            em.getTransaction().begin();
                SencillaColeccion sc = em.find(SencillaColeccion.class,id);
            em.remove(sc);
            em.getTransaction().commit();

            
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener la editorial", ex);     
        } finally {
            em.close(); 
        }      
    }

    public List<SencillaColeccion> obtenerTodasSencillaColeccion() throws DAOException {
    EntityManager em = emf.createEntityManager();
      try{
      em.getTransaction().begin();
      em.flush();
      em.getTransaction().commit();
      //Para realizar consultas usaremos el lenguaje JPQL, muy parecido a HQL y a SQL
      //Al igual que HQL recuerda que no hacemos consultas sobre tablas sino sobre objetos
      Query query = em.createQuery("SELECT s FROM SencillaColeccion s ");
      //En caso de necesitar parámetros los ontroducimos así en la consulta sería con : igual que HQL
      //query.setParameter("parametro",valor);
      List sc = query.getResultList();
      return sc;
      } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al obtener las Obras SencillaColeccion", ex);
        } finally {
            em.close();
            
        }
    }
    public int modificaSencillaColeccion(String ISBN,SencillaColeccion s) throws DAOException {
        
        EntityManager em = emf.createEntityManager();
         try{
                em.getTransaction().begin();
                em.flush();
                
                SencillaColeccion identSc=this.obtenerSencillaColeccion(ISBN);
                long id = identSc.getId();
                
                SencillaColeccion sc = em.find(SencillaColeccion.class,id);
                
                sc.setNombre(s.getNombre());
                sc.setAutor(s.getAutor());
                sc.setIdioma(s.getIdioma());
                sc.setDerechos(s.getDerechos());
                sc.setEditorial(s.getEditorial());
                sc.setTipo(s.getTipo());
                sc.setSubtipoObra(s.getSubtipoObra());
                sc.setDescripcion(s.getDescripcion());
                sc.setArchivo(s.getArchivo());
                
                
                em.persist(sc);
                em.getTransaction().commit();
                
                return 1;
                                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al Modificar la Obra SencillaColeccion", ex);
                
        } finally {
            em.close(); 
        }

    }
     
     public void finalizar() throws DAOException {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }

    public List<SencillaColeccion> obtenerTodasSencillasColeccionEditorial(String cif) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            Editorial e = e_idao.obtenerEditorial(cif);
            long id_editorial = e.getId();
            
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT sc FROM SencillaColeccion sc WHERE sc.editorial.id=:id_editorial");
            query.setParameter("id_editorial",id_editorial);
         
            List<SencillaColeccion> sc= query.getResultList();
            
            return sc;
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener las obras de colección sencillas", ex);
        } finally {
            em.close();
        }
    }

    
    
}
