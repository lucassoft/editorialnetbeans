package com.lucassoft.editorialjpa.service;

import java.util.ArrayList;
import java.util.List;
import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.EditorialJPADAO;
import com.lucassoft.editorialjpa.datos.SencillaColeccionJPADAO;
import com.lucassoft.editorialjpa.datos.SencillaNormalJPADAO;
import com.lucassoft.editorialjpa.entidades.SencillaColeccion;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;

public class ServicioEditorial 
{
    EditorialJPADAO idao=null;
    SencillaNormalJPADAO ssn = null;
    SencillaColeccionJPADAO ssc = null;

    public ServicioEditorial() throws DAOException
    {
        idao = new EditorialJPADAO();
        ssn = new SencillaNormalJPADAO();
        ssc = new SencillaColeccionJPADAO();
    }


    public void nuevaEditorial(String email, String password, String nombre, String cif, String RazSoc, String tel1, String tel2, String cuentaCo, String pais) throws ServicioException, DAOException
    {
        Editorial e = new Editorial();
        
        List<Editorial>editoriales = idao.obtenerTodasEditoriales();
        if(editoriales.isEmpty())
            e.setId(0);
        else
        {
            long id= editoriales.get(editoriales.size()-1).getId()+1;
            e.setId(id);
        }
        
        e.setEmail(email);
        e.setPassword(password);
        e.setNombre(nombre);
        e.setCif(cif);
        e.setRazonSocial(RazSoc);
        e.setTelefono1(tel1);
        e.setTelefono2(tel2);
        e.setCuentaCorriente(cuentaCo);
        e.setPais(pais);

        Editorial ePrueba=idao.obtenerEditorial(cif);

        if(ePrueba!=null)
            throw new ServicioException("Ya existe una editorial con ese cif");

        Editorial ePruebaEmail=idao.obtenerEditorialEmail(e.getEmail());

        if(ePruebaEmail!=null)
                throw new ServicioException("Ya existe una editorial con ese email");

        idao.nuevaEditorial(e);
    }


    public Editorial obtenerEditorial(String cif) throws DAOException
    {
        Editorial e = idao.obtenerEditorial(cif);

        return e;
    }



    public List<Editorial> obtieneEditoriales() throws DAOException
    {
        List<Editorial> editoriales= idao.obtenerTodasEditoriales();

        if(editoriales==null)
            editoriales=new ArrayList<Editorial>();

         return editoriales;
    }



    public void borraEditorial(String cif) throws DAOException, ServicioException
    {
        List<SencillaNormal> sn = ssn.obtenerTodasSencillasEditorial(cif);
        List<SencillaColeccion> sc = ssc.obtenerTodasSencillasColeccionEditorial(cif);
        
        if(!sn.isEmpty() || !sc.isEmpty())
            throw new ServicioException("La editorial aun tiene obras y no se puede borrar.");
        
        Editorial e = idao.obtenerEditorial(cif);
        if(e==null)
            throw new ServicioException("La editorial que quieres borrar no existe.");
        else
            idao.eliminarEditorial(cif);
    }




    public void modificaEditorial(String cif, String password, String nombre, String RazSoc, String tel1, String tel2, String cuentaCo, String pais) throws DAOException, ServicioException
    {
        Editorial e = new Editorial();

        e.setPassword(password);
        e.setNombre(nombre);
        e.setRazonSocial(RazSoc);
        e.setTelefono1(tel1);
        e.setTelefono2(tel2);
        e.setCuentaCorriente(cuentaCo);
        e.setPais(pais);

        Editorial ePrueba=idao.obtenerEditorial(cif);

        if(ePrueba==null)
            throw new ServicioException("No existe una editorial con ese cif");

        int afectadas = idao.modificaEditorial(cif,e);

        if (afectadas==0)
            throw new ServicioException("No es ha modificado ninguna editorial.");
    }
}
