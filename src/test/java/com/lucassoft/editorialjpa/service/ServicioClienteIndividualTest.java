/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.lucassoft.editorialjpa.service;


import com.lucassoft.editorialjpa.datos.ClienteIndividualJPADAOTEST;
import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.entidades.ClienteIndividual;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author user
 */
public class ServicioClienteIndividualTest {
    
    //Fíjate son variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static ServicioClienteIndividual sf=null;
	
	//Vamos a probar la clase de servicio usando la implementación de JDBC
	//que es la única que va a tener test
	//como es lógico hasta que no se pasen todos los test de JDBCDAO
	//No deberemos empezar a preguntarnos por qué fallan estos otros
	//q seguro q lo haran
	
	//Este método se ejucuta UNA vez, justo antes de empezar las pruebas
        @BeforeClass
	public static void inicializarBD() throws Exception {
            

        try {
       	 //Ojo esta es una clase de test obtendrá la configuración
        //del .properties de src/TEST/resources NO de src/main/resources
         sf= new ServicioClienteIndividual();
       	 Properties pro = new Properties();
   		 pro.load(ClienteIndividualJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            sf= new ServicioClienteIndividual();
            
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	      
	     
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
        
        //Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
	}
        //Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
	
	//Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	@After
	public void tearDown() throws Exception {
	}
	
	
	//Anotamos con @Test todos los test que queramos hacer, generalmnte uno
	//por método.Antes de iniciar el test la bd está en un estado conocido
	//es decir la base de datos del test está tal y como la ha configurado el dataset-dao.xml
	//tras su ejecución por parte de dbunit

	@Test
	public void testNuevoClienteIndividual() {
		try {
			SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
			Date d= sdf.parse("20/5/1996");
                        
                        String[] array_obrasindividuales;
                        array_obrasindividuales = new String[1];
                        array_obrasindividuales[0]="441-55";
                        
			sf.nuevoClienteIndividual("8@gmail.com", "ed8", "Pepe", "España", "Alicante", "Sax", "03657", "C/ Mayor 6", d, array_obrasindividuales);
			List<ClienteIndividual> clienteindividual = sf.obtieneClientesIndividuales();
			if (clienteindividual.size() != 6) {//Hay que crear 5 editoriales de ejemplo
				fail("No se ha insertado el cliente individual");
			}
			for (ClienteIndividual j : clienteindividual) {
				assertNotNull(j.getEmail());
                                assertNotNull(j.getPassword());
                                assertNotNull(j.getNombre());
                                assertNotNull(j.getPais());
                                assertNotNull(j.getProvincia());
                                assertNotNull(j.getLocalidad());
                                assertNotNull(j.getCodigopostal());
                                assertNotNull(j.getDireccion());
                                assertNotNull(j.getFechaNacimiento());
                               // assertNotNull(j.getSubgrupo());
                                assertNotNull(j.getObrasIndividuales());
                                
                        
			}

		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han insertado el cliente individual al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}

	}
        /*
        @Test(expected = ServicioException.class)
        public void testNuevoClienteIndividualConEmailRepetido() throws Exception {
            SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
            Date d= sdf.parse("20/5/1996");
            sf.nuevoClienteIndividual("1@gmail.com", "hjfg8", "Adolfo", "Italia", "Alicante", "Sax", "03689", "C/ Mayor 98", d);

        }
        */
        
        @Test
	public void testModificarClienteIndividual(){
            try
            {
                SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
		Date d= sdf.parse("12/00/1994");
                sf.modificaClienteIndividual("6@email.com", "1", "1", "1", "cc1", "pais", "rs1", "1", d);
            
            } catch (ParseException e) {
			fail("TEST NO SUPERADO: No se ha modificado el cliente individual al producirse una excepcion: "
					+ e.getLocalizedMessage());
            } catch (DAOException e) {
                fail("TEST NO SUPERADO: No se ha modificado el cliente individual al producirse una excepcion: "
                        + e.getLocalizedMessage());
            } catch (ServicioException e) {
                fail("TEST NO SUPERADO: No se ha modificado el cliente individual al producirse una excepcion: "
                        + e.getLocalizedMessage());
            }
	}
        
        
        @Test
	public void testObtenerTodosClientesIndividuales() {
		try {

			List<ClienteIndividual> clienteindividual = sf.obtieneClientesIndividuales();
			if (clienteindividual.size() != 5) {
				fail("No se han obtenido todos los clientes individuales");
			}
			for (ClienteIndividual j : clienteindividual)  {
				assertNotNull(j.getEmail());
                                assertNotNull(j.getPassword());
                                assertNotNull(j.getNombre());
                                assertNotNull(j.getPais());
                                assertNotNull(j.getProvincia());
                                assertNotNull(j.getLocalidad());
                                assertNotNull(j.getCodigopostal());
                                assertNotNull(j.getDireccion());
                                assertNotNull(j.getFechaNacimiento());
                                //assertNotNull(j.getSubgrupo());
                                //assertNotNull(j.getObrasindividuales());
			}
		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han obtenido los clientes individuales al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}
        
        @Test
	public void testEliminarClienteIndividual() {
		try {
			//¿Que ocurre si decidimos eliminar un jugador que no existe o con id negativo?
			//según esta implementación no se hace nada, ya que no añadimos un test fail, para estos casos
			sf.borraClienteIndividual();
			List<ClienteIndividual> clienteindividual = sf.obtieneClientesIndividuales();
			if (clienteindividual.size() != 5) {
				fail("No se ha eliminado el cliente individual");
			}
			for (ClienteIndividual j : clienteindividual)  {
				assertNotNull(j.getEmail());
                                assertNotNull(j.getPassword());
                                assertNotNull(j.getNombre());
                                assertNotNull(j.getPais());
                                assertNotNull(j.getProvincia());
                                assertNotNull(j.getLocalidad());
                                assertNotNull(j.getCodigopostal());
                                assertNotNull(j.getDireccion());
                                assertNotNull(j.getFechaNacimiento());
                                //assertNotNull(j.getSubgrupo());
                                //assertNotNull(j.getObrasindividuales());
			}
			
		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se ha eliminado el cliente individual al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}
	
}
