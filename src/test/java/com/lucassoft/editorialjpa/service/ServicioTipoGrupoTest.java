/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.entidades.TipoGrupo;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Abraham
 */
public class ServicioTipoGrupoTest {
    
     private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ServicioTipoGrupo sf=null;
    
    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            sf= new ServicioTipoGrupo();
            
            Properties pro = new Properties();
            pro.load(ServicioTipoObraTest.class.getResourceAsStream("/configuracion.properties"));
            bd=pro.getProperty("bdJDBC");
            login=pro.getProperty("loginJDBC");
            password=pro.getProperty("passwordJDBC");
            url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                                        .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            
            sf= new ServicioTipoGrupo();
            
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("test-DAO-dataset.xml"));
              
        } catch (InstantiationException ex) {
            System.out.println("Hubo un problema al iniciar la base de datos del test: ");
            ex.printStackTrace();
        } catch (IllegalAccessException iae) {
            System.out.println("Hubo un problema al acceder a la base de datos. del test");
            iae.printStackTrace();
        } catch (SQLException sqle) {
            System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
            sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
            System.out.println("No se encuentra el driver JDBC."+cnfe);
            cnfe.printStackTrace();
        } catch (IOException ioe) {
            System.out.println("Error al accerder al archivo de configuración: "+ioe);
            ioe.printStackTrace();
        }catch (Exception ex) {
            System.out.println("Error inesperado al iniciar los test");
            ex.printStackTrace();
        }
    }
    
     @AfterClass
    public static  void finalizar() throws Exception{
            if(connection!=null){
                    connection.close();
            }
    }

    @Before
    public void setUp() throws Exception{
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }

    @After
    public void tearDown() throws Exception {
    }

    
    @Test
    public void testNuevoTipoGrupo()
    {
        try
        {
            sf.nuevoTipoGrupo(1, "Drama");
            List<TipoGrupo> tipogrupo = sf.obtenerTodosTipoGrupo();
            
            if (tipogrupo.size() != 6) 
                fail("No se ha insertado el TipoGrupo");
            

            for (TipoGrupo t : tipogrupo) 
            {
                assertNotNull(t.getMaximoComponentes());
                assertNotNull(t.getDescripcion());
            }
                
        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se ha insertado el Tipogrupo al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }
 @Test(expected = ServicioException.class)
    public void testNuevoTipoGrupoConDescripcionRepetidaFail() throws Exception 
    {
        sf.nuevoTipoGrupo(1, "Drama");
    }

    
    @Test
    public void testObtenerTipoGrupo()
    {
        try 
        {
            TipoGrupo to = sf.obtenerTipoGrupo("Drama");
            
            assertNotNull(to.getDescripcion());
            
        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se han obtenido los tipoGrupo al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }

    
    @Test
    public void testObtenerTodosTipoGrupo()
    {
        try 
        {
            List<TipoGrupo> tiposgrupo = sf.obtenerTodosTipoGrupo();
            
            if (tiposgrupo.size() != 5) 
                    fail("No se han obtenido todas los tipoGrupo");
            
            
            for (TipoGrupo t : tiposgrupo) 
            {
                    assertNotNull(t.getMaximoComponentes());
                    assertNotNull(t.getId());
                    assertNotNull(t.getDescripcion());
            }
            
        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se han obtenido los tipoGrupo al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }
    
    
    
     @Test   
    public void testElimitarTipoGrupo()
    {        
        try 
        {
            sf.eliminarTipoGrupo("Drama");
            List<TipoGrupo> tipogrupo = sf.obtenerTodosTipoGrupo();
            
            if (tipogrupo.size() != 4) 
                    fail("No se ha eliminado el tipoObra");

            for (TipoGrupo t : tipogrupo) 
            {
                assertNotNull(t.getMaximoComponentes());
                assertNotNull(t.getId());
                assertNotNull(t.getDescripcion());
            }
            
        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se ha eliminado el TipoGrupo al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }
    
     @Test   
    public void testModificaTipoGrupo()
    {        
        try 
        {
            sf.modificaTipoGrupo(1,"terror");
            TipoGrupo to = sf.obtenerTipoGrupo("Drama");
            
            assert(to.getDescripcion().equals("terror"));

        } catch (Exception e) {
            fail("TEST NO SUPERADO: No se ha eliminado el TipoGrupo al producirse una excepcion: "
                    + e.getLocalizedMessage());
        }
    }
}
