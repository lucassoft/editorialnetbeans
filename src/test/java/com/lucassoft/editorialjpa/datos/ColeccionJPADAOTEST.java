/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Coleccion;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author user
 */
public class ColeccionJPADAOTEST {
    
    //variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static IColeccionDAO idao=null;
        private static IEditorialDAO idaoed=null;
        private static ITipoObraDAO idaoto=null;
	
	
	//Este metodo se ejucuta UNA vez, justo antes de empezar las pruebas
	@BeforeClass
	public static void inicializarBD() throws Exception {
		

        try {
       	 //Ojo esta es una clase de test obtendrÃ¡ la configuraciÃ³n
        //del .properties de src/TEST/resources NO de src/main/resources
       	 Properties pro = new Properties();
   		 pro.load(ColeccionJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

       	 
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));////// falta hacer el obraDAO-dataset
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquÃ­ la instucciÃ³n del new 
	         //en el constructor coge la configuraciÃ³n de la bd de los recursos de test
	         idao= new ColeccionJPADAO();
                 idaoed=new EditorialJPADAO();
                 idaoto=new TipoObraJPADAO();
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuraciÃ³n: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
        
        @AfterClass
        public static  void finalizar() throws Exception
        {
            if(connection!=null)
                connection.close();

            idao.finalizar();
        }
        
        @Before
        public void setUp() throws Exception
        {
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

        }  
    
        @After
        public void tearDown() throws Exception {
        }

        
        @Test
        public void testObtenerTodasColecciones() 
        {
            try
            {
                List<Coleccion> colecciones= idao.obtenerTodasColecciones();
                assert(colecciones.size()==5);

                for(Coleccion col :colecciones)
                        assertNotNull(col.getISBN());

                Coleccion col=colecciones.get(0);

                assert(col.getISBN().equals("44-521"));
                assert(col.getNombre().equals("antonia"));
                assert(col.getAutor().equals("paquito"));
                assert(col.getIdioma().equals("chino"));
                assert(col.getDerechos()==true);
                assert(col.getEditorial().getCif().equals("2"));
                assert(col.getTipo().getNombre().equals("tipoobra2"));
                assert(col.getSubtipoObra().equals("chunga"));
                assert(col.getDescripcion().equals("Mola mazo"));
                assert(col.getDescripcionColeccion().equals("chulachula"));
                //Falta el campo "obras", que es una lista

                Coleccion colec = null;

                for(Coleccion col2 : colecciones)
                    if(col2.getISBN().equals("44-521"))
                        colec=col2;

                assertNotNull(colec);
                assert(colec.getNombre().equals("antonia"));
                /*assertNotNull(colec.getEditorial());
                assertNotNull(colec.getTipo());*/

            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de colecciones al producirse una excepcion: "+e.getLocalizedMessage());
            }
        }
        
        @Test
        public void testNuevaColeccion() 
        {
            try
            {
                Coleccion col= new Coleccion();
                
                List<Coleccion> colecciones = idao.obtenerTodasColecciones();
                long id= colecciones.get(colecciones.size()-1).getId()+1;
                
                col.setId(id);
                col.setISBN("ISBNHP1");
                col.setNombre("Harry Potter");
                col.setAutor("JK Rowling");
                col.setIdioma("Inglés");
                col.setDerechos(true);

                
                Editorial e = idaoed.obtenerEditorialEmail("2@email.com");
                col.setEditorial(e);

                TipoObra tp = idaoto.obtenerTipoObra("tipoobra2");
                col.setTipo(tp);

                col.setSubtipoObra("Saga fantástica - magos");
                col.setDescripcion("Harry Potter es cool y hace stuff como Jesús");
                col.setDescripcionColeccion("Penosa, ni gratis la quiero");
                
                idao.nuevaColeccion(col);

                colecciones=idao.obtenerTodasColecciones();
                Coleccion colec=null;

                for(Coleccion col2 :colecciones)
                        if(col2.getISBN().equals("ISBNHP1"))
                                colec=col2;

                assertNotNull(colec);
                assert(colec.getISBN().equals("ISBNHP1"));
                assert(colec.getNombre().equals("Harry Potter"));
                assert(colec.getAutor().equals("JK Rowling"));
                assert(colec.getIdioma().equals("Inglés"));
                assert(colec.getDerechos()==true);
                assert(colec.getEditorial().getEmail().equals("2@email.com"));
                assert(colec.getTipo().getNombre().equals("tipoobra2"));
                assert(colec.getSubtipoObra().equals("Saga fantástica - magos"));
                assert(colec.getDescripcion().equals("Harry Potter es cool y hace stuff como Jesús"));
                assert(colec.getDescripcionColeccion().equals("Penosa, ni gratis la quiero"));
                //Falta el campo "obras", que es una lista


            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha insertado la coleccion al producirse una excepcion: "+e.getLocalizedMessage());
            }
        }
        
        
        @Test
        public void testObtenerColeccion() 
        {
            try
            {
                Coleccion col=idao.obtenerColeccion("44-552");
                

                if(col.getISBN()==null)
                        fail("No se ha obtenido la colección apropiadamente, la coleccion no tiene ISBN");

                if(!(col.getNombre().equals("antonia")))
                        fail("No se ha obtenido la coleccion apropiadamente, el nombre no es el correcto");

                if(!(col.getAutor().equals("paquito")))
                        fail("No se ha obtenido la coleccion apropiadamente, el autor no es el correcto");

                if(!(col.getIdioma().equals("chino")))
                        fail("No se ha obtenido la coleccion apropiadamente, el idioma no es el correcto");

                if(!(col.getDerechos()==true))
                        fail("No se ha obtenido la coleccion apropiadamente, los derechos no son los correctos");

                if(col.getEditorial()==null)
                        fail("No se ha obtenido la coleccion apropiadamente, la coleccion no tiene editorial");

                if(col.getTipo()==null)
                        fail("No se ha obtenido la coleccion apropiadamente, la coleccion no tiene tipo obra");

                if(!(col.getSubtipoObra().equals("chunga")))
                        fail("No se ha obtenido la coleccion apropiadamente, el subtipo de obra no es el correcto");

                if(!(col.getDescripcion().equals("Mola mazo")))
                        fail("No se ha obtenido la coleccion apropiadamente, la descripción no es la correcta");

                if(!(col.getDescripcionColeccion().equals("chulachula")))
                        fail("No se ha obtenido la coleccion apropiadamente, el precio de la coleccion no es el correcto");

                //Falta el campo obras
                
            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha podido obtener la coleccion al producirse una excepcion: "+e.getLocalizedMessage());
            }
        }
        
        
        @Test
        public void testModificaColeccion() 
        {
            try
            {
                Coleccion col= new Coleccion();

                    
                col.setNombre("Prueba");
                col.setAutor("Prueba");
                col.setIdioma("Español");
                col.setDerechos(false);

                Editorial e = idaoed.obtenerEditorialEmail("3@email.com");
                col.setEditorial(e);

                TipoObra tp = idaoto.obtenerTipoObra("tipoobra3");
                col.setTipo(tp);
                
                col.setSubtipoObra("prueba");
                col.setDescripcion("modifica_prueba");
                col.setDescripcionColeccion("Penosa, ni gratis la quiero");
                
                //Fatla el campo "obras"

                idao.modificaColeccion("44-554",col);

                Coleccion col_mod = idao.obtenerColeccion("44-554");

                assert(col_mod.getNombre().equals("Prueba"));
                assert(col_mod.getAutor().equals("Prueba"));
                assert(col_mod.getIdioma().equals("Español"));
                assert(col_mod.getDerechos()==false);
                assert(col_mod.getEditorial().getEmail().equals("3@email.com"));
                assert(col_mod.getTipo().getNombre().equals("tipoobra3"));
                assert(col_mod.getSubtipoObra().equals("prueba"));
                assert(col_mod.getDescripcion().equals("modifica_prueba"));
                assert(col_mod.getDescripcionColeccion().equals("chulachula"));


            }catch(Exception e){
                fail("TEST NO SUPERADO: No se ha podido modificar la coleccion al producirse una excepcion: "+e.getLocalizedMessage());
            }
        }
        
        
        @Test
        public void testEliminaColeccion() 
        {
            try
            {
                idao.eliminarColeccion("44-555");

                List<Coleccion> colecciones=null;
                colecciones=idao.obtenerTodasColecciones();

                if(!(colecciones.size()==4))
                    fail("TEST NO SUPERADO: No se ha eliminado la coleccion");

                if(idao.obtenerColeccion("441-55")!=null)
                    fail("TEST NO SUPERADO: No se ha eliminado la coleccion seleccionada");

            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha podido obtener la lista de colecciones al producirse una excepcion: "+e.getLocalizedMessage());
            }
        }
        
        
        
        
        
        
}
