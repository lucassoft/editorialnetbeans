package com.lucassoft.editorialjpa.service;

import java.util.ArrayList;
import java.util.List;
import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.TipoObraJPADAO;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServicioTipoObra 
{
    TipoObraJPADAO idao=null;

    public ServicioTipoObra() throws DAOException
    {
        idao=new TipoObraJPADAO();   
    }

    public void nuevoTipoObra(String nombre, String descripcion) throws ServicioException, DAOException
    {
       
        TipoObra t = new TipoObra();

         List<TipoObra> tipoObra=idao.obtenertodosTipoObra();
        if(tipoObra.isEmpty())
            t.setId(0);
        else
        {
            long id=tipoObra.get(tipoObra.size()-1).getId()+1;
            t.setId(id);
        }
             
        t.setNombre(nombre);
        t.setDescripcion(descripcion);

        TipoObra ePrueba=idao.obtenerTipoObra(t.getNombre());//falta la query del update
            
        if(ePrueba!=null)
            throw new ServicioException("Ya existe un TipoObra con ese cif");

        idao.nuevoTipoObra(t);
    }

    public TipoObra obtenerTipoObra(String nombre) throws DAOException
    { 
        TipoObra t= idao.obtenerTipoObra(nombre);

        return t;
    }



    public List<TipoObra> obtenerTodosTipoObra() throws DAOException
    {
        List<TipoObra> tipoobra= idao.obtenertodosTipoObra();

        if(tipoobra==null)
            tipoobra=new ArrayList<TipoObra>();

        return tipoobra;
    }



    public void borraTipoObra(String nombre) throws  DAOException
    {	
        try 
        {
            TipoObra tipoobra = idao.obtenerTipoObra(nombre);

            if (tipoobra==null)
                throw new ServicioException("El tipo de obra no existe.");

            int resultado = idao.eliminarTipoObra(nombre);

            if (resultado!=1)
                throw new ServicioException("No se ha eliminado el tipo obra "+ nombre);

        } catch (ServicioException ex) {
            Logger.getLogger(ServicioTipoObra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void modificaTipoObra(String nombre, String descripcion) throws DAOException, ServicioException
    { 	   
        int afectadas = idao.modificaTipoObra(nombre, descripcion);

        if (afectadas == 0)
            throw new ServicioException("No es ha modificado ningun tipoObra.");
    }
}
