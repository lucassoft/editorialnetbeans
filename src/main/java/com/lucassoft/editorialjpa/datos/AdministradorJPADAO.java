/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Administrador;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Kiko
 */
public class AdministradorJPADAO implements IAdministradorDAO{
        EntityManagerFactory emf=null;

    
    
    public AdministradorJPADAO() throws DAOException {
        try{
            //Creamos la factoria
        emf= Emf.getInstance().getEmf();
        
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        }
        
    }
    
    
    public void nuevoAdministrador(Administrador a) throws DAOException {
        

        // Creamos el entity Manager
         
        EntityManager em = emf.createEntityManager();

        // Marcamos el comienzo de la transacción
        em.getTransaction().begin();
        try {         

            // Lo añadimos a la BD
            em.persist(a);
           
            //Hacemos commit es decir guardamos los cambios
            em.getTransaction().commit();
        } catch (Exception ex) {
            //Si ha habido cualquier problema podemos volver atrás la transacción
            //esto será util cuando haganos varias cosas en una única transaccion
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar el Administrador", ex);
            
            
        } finally {
            em.close();
            
        }
    }
    
    public int eliminarAdministrador(String email) throws DAOException {
          
     
       EntityManager em = emf.createEntityManager();
         em.getTransaction().begin();
         Administrador ad = this.obtenerAdministradorEmail(email);
         long id = ad.getId();
         
         try{
         //Mediante find obtenemos un objeto a partir de su clave primaria
         Administrador a=em.find(Administrador.class,id);
         //Así borramos el jugador
         em.remove(a);
         em.getTransaction().commit();
         return 1;
         } catch (Exception ex) {

           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al eliminar el Administrador", ex);
        } finally {
            em.close();
            
        }
         
    }
    
     public Administrador obtenerAdministradorEmail(String email) throws DAOException {
         EntityManager em = emf.createEntityManager();
         try{
                Administrador a = null;
                em.getTransaction().begin();
                em.flush();
                em.getTransaction().commit();
                
                Query query = em.createQuery("SELECT a FROM Administrador a WHERE a.email=:email");
                query.setParameter("email",email);
                List Administrador=query.getResultList();
                List<Administrador> edit=(List<Administrador>) Administrador;
                if(edit.isEmpty()){
                    return null;
                }
                return edit.get(0);
                    
                
                
                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener el Administrador", ex);
                
        } finally {
            em.close(); 
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Administrador> obtenerTodosAdministradores() throws DAOException {
      EntityManager em = emf.createEntityManager();
      try{
      em.getTransaction().begin();
      em.flush();
      em.getTransaction().commit();
      //Para realizar consultas usaremos el lenguaje JPQL, muy parecido a HQL y a SQL
      //Al igual que HQL recuerda que no hacemos consultas sobre tablas sino sobre objetos
      Query query = em.createQuery("SELECT a FROM Administrador a ");
      //En caso de necesitar parámetros los ontroducimos así en la consulta sería con : igual que HQL
      //query.setParameter("parametro",valor);
      List ad = query.getResultList();
      return ad;
      } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al obtener los Administradores", ex);
        } finally {
            em.close();
            
        }
    }
    
    public void finalizar() throws DAOException {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }

    public int modificaAdministrador(String email, Administrador a) throws DAOException {
         EntityManager em = emf.createEntityManager();
         try{
                em.getTransaction().begin();
                em.flush();
                
                Administrador identAdm=this.obtenerAdministradorEmail(email);
                long id = identAdm.getId();
                
                Administrador adm = em.find(Administrador.class,id);
                
                adm.setNombre(a.getNombre());
                adm.setPassword(a.getPassword());
                adm.setEmail(a.getEmail());
                
                em.persist(adm);
                em.getTransaction().commit();
                
                return 1;
                                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al modificar el Administrador", ex);
                
        } finally {
            em.close(); 
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    



}
   
