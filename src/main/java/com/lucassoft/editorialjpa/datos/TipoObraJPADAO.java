package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class TipoObraJPADAO implements ITipoObraDAO
{
    EntityManagerFactory emf=null;
    
    public TipoObraJPADAO() throws DAOException 
    {
        try
        {
            emf= Emf.getInstance().getEmf();
        
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        }
    }
     
    public void nuevoTipoObra(TipoObra t) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try 
        {
            em.persist(t);
            em.getTransaction().commit();
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al guardar el tipo de obra", ex);  
        } finally {
            em.close(); 
        }
    }
      
      
    public int eliminarTipoObra(String nombre) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        try
        {
            TipoObra identTO=this.obtenerTipoObra(nombre);
            long id = identTO.getId();
         
            em.getTransaction().begin();
                TipoObra to = em.find(TipoObra.class,id);
            em.remove(to);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el tipo obra", ex);     
        } finally {
            em.close(); 
        } 
    }
      
      
    public TipoObra obtenerTipoObra(String n)throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            TipoObra e = null;
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT e FROM TipoObra e WHERE e.nombre=:nombre");
            query.setParameter("nombre",n);
            List tipoobraRes=query.getResultList();
            List<TipoObra> edit=(List<TipoObra>) tipoobraRes;

            if(edit.size()==0)
                return null;

            return edit.get(0);
             
        } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al obtener el tipo de obra", ex);
        } finally {
            em.close();   
        }
    }
      
      
      
    public List<TipoObra> obtenertodosTipoObra() throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT to FROM TipoObra to ");

            List to = query.getResultList();
            return to;
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener las editoriales", ex);
        } finally {
            em.close();
        }
    }
      
      
    public void finalizar() throws DAOException 
    {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }

   /* public TipoObra obtenerTipoObra(long id) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            TipoObra j=em.find(TipoObra.class,id);

             return j;
             
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el tipo de obra", ex);
        } finally {
            em.close(); 
        }
    }  */
    
    public int modificaTipoObra(String nombre, String descrip) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        try
        {

            TipoObra identTO=this.obtenerTipoObra(nombre);
            long id = identTO.getId();
            
            em.getTransaction().begin();
            em.flush();
            
                TipoObra to = em.find(TipoObra.class,id);
 
            to.setDescripcion(descrip);
            
            em.persist(to);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el tipo obro", ex);      
        } finally {
            em.close(); 
        }
    }
}
