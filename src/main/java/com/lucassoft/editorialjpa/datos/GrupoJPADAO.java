/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;



import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.Grupo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Abraham
 */
public class GrupoJPADAO implements IGrupoDAO {
    
           EntityManagerFactory emf=null;

    
    
    public GrupoJPADAO() throws DAOException {
        try{
            //Creamos la factoria
        emf= Emf.getInstance().getEmf();
        
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        }
        
    }
    

    public void nuevoGrupo(Grupo g) throws DAOException {
        // Creamos el entity Manager
         
        EntityManager em = emf.createEntityManager();

        // Marcamos el comienzo de la transacción
        em.getTransaction().begin();
        try {
           

            // Lo añadimos a la BD
            em.persist(g);
           
            //Hacemos commit es decir guardamos los cambios
            em.getTransaction().commit();
        } catch (Exception ex) {
            //Si ha habido cualquier problema podemos volver atrás la transacción
            //esto será util cuando haganos varias cosas en una única transaccion
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar el grupo", ex);
            
            
        } finally {
            em.close();
            
        }
    }

    public void finalizar() throws DAOException {
       //Hay que cerrar el Entity Manager Factory
        emf.close();
    }

    public Grupo obtenerGrupo(long id) throws DAOException {
        EntityManager em = emf.createEntityManager();
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Grupo g=em.find(Grupo.class,id);

             return g;
             
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el grupo", ex);
        } finally {
            em.close(); 
        }
    }

    public Grupo obtenerGrupoEmail(String email) throws DAOException {
            EntityManager em = emf.createEntityManager();
         try{
                Grupo g = null;
                em.getTransaction().begin();
                em.flush();
                em.getTransaction().commit();
                
                Query query = em.createQuery("SELECT g FROM Grupo g WHERE g.email=:email");
                query.setParameter("email",email);
                List Grupo=query.getResultList();
                List<Grupo> edit=(List<Grupo>) Grupo;
                if(edit.size()==0){
                    return null;
                }
                return edit.get(0);
                    
                
                
                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener el grupo", ex);
                
        } finally {
            em.close(); 
        }
    }

    public int eliminarGrupo(String email) throws DAOException {
          EntityManager em = emf.createEntityManager();
        try
        {
            Grupo identGrupo=this.obtenerGrupoEmail(email);
            long id = identGrupo.getId();
         
            em.getTransaction().begin();
                Grupo g = em.find(Grupo.class,id);
            em.remove(g);
            em.getTransaction().commit();
             return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener el grupo", ex);     
        } finally {
            em.close(); 
        } 
    }

    public List<Grupo> obtenerTodosGrupos() throws DAOException {
        
           EntityManager em = emf.createEntityManager();
      try{
      em.getTransaction().begin();
      em.flush();
      em.getTransaction().commit();
      //Para realizar consultas usaremos el lenguaje JPQL, muy parecido a HQL y a SQL
      //Al igual que HQL recuerda que no hacemos consultas sobre tablas sino sobre objetos
      Query query = em.createQuery("SELECT g FROM Grupo g ");
      //En caso de necesitar parámetros los ontroducimos así en la consulta sería con : igual que HQL
      //query.setParameter("parametro",valor);
      List gr = query.getResultList();
      return gr;
      } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al obtener los grupos", ex);
        } finally {
            em.close();
            
        }
    }

    public int modificaGrupo(String email, Grupo g) throws DAOException {
        
         EntityManager em = emf.createEntityManager();
         try{
             
             Grupo idGrupo=this.obtenerGrupoEmail(email);
                long id = idGrupo.getId();
                em.getTransaction().begin();
                em.flush();
                
                 
                
                Grupo gru = em.find(Grupo.class,id);
                
                gru.setNombre(g.getNombre());
                gru.setPassword(g.getPassword());
                gru.setEmail(g.getEmail());
                gru.setPais(g.getPais());
                gru.setProvincia(g.getProvincia());
                gru.setLocalidad(g.getLocalidad());
                gru.setCodigopostal(g.getCodigopostal());
                gru.setDireccion(g.getDireccion());
                gru.setNombreResponsable(g.getNombreResponsable());
                gru.setCargoResponsable(g.getCargoResponsable());
                
                
                em.persist(gru);
                em.getTransaction().commit();
                
                return 1;
                                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener el grupo", ex);
                
        } finally {
            em.close(); 
        }
    }
    
}
