/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Kraftwerk
 */
public class Emf {
    //La única instancia d esta clase se crea al iniciar la ejecucion
    private static Emf miUnicaInstancia =new Emf();
    //Aquí debe ir el nombre de la unidad de persistencia definida en persistence.xml
    
    static private final String PERSISTENCE_UNIT_NAME = "editorialjpa";
    
    private EntityManagerFactory emf = null;
    
    //Fíjate,el constructor es privado
    private Emf() {
    }
    
    //Mediante este método accedemos a la única instancia de la clase
    public static Emf getInstance() {
        return miUnicaInstancia;
    }

    //Este método nos devuelve la factoría como ves solo la crea una vez
    //Y será cuando se llame por primera vez al emf
    public EntityManagerFactory getEmf() {
        if (this.emf == null || !this.emf.isOpen())
        {
            this.emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
        }
        return this.emf;
    }
    
}
