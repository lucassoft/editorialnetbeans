/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.AdministradorJPADAOTEST;
import com.lucassoft.editorialjpa.datos.EditorialJPADAOTEST;
import com.lucassoft.editorialjpa.entidades.Administrador;
import com.lucassoft.editorialjpa.entidades.Editorial;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author saul
 */
public class ServicioAdministradorTest {
    
    private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ServicioAdministrador sf=null;
    
    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            sf= new ServicioAdministrador();
            Properties pro = new Properties();
            pro.load(AdministradorJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
                    bd=pro.getProperty("bdJDBC");
                    login=pro.getProperty("loginJDBC");
                    password=pro.getProperty("passwordJDBC");
                    url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                       .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);


            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            sf= new ServicioAdministrador();

            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("test-DAO-dataset.xml"));

        } catch (InstantiationException ex) {
                System.out.println("Hubo un problema al iniciar la base de datos del test: ");
                ex.printStackTrace();
        } catch (IllegalAccessException iae) {
                System.out.println("Hubo un problema al acceder a la base de datos. del test");
                iae.printStackTrace();
        } catch (SQLException sqle) {
                System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
                sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
                System.out.println("No se encuentra el driver JDBC."+cnfe);
                cnfe.printStackTrace();
        } catch (IOException ioe) {
                System.out.println("Error al accerder al archivo de configuración: "+ioe);
                ioe.printStackTrace();
        } catch (Exception ex) {
                System.out.println("Error inesperado al iniciar los test");
                ex.printStackTrace();
        }
   }
    
    @AfterClass
    public static  void finalizar() throws Exception{
            if(connection!=null){
                    connection.close();
            }
    }


    @Before
    public void setUp() throws Exception{
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    @Test
    public void testNuevoAdministrador()
    {
        try 
        {
            sf.nuevoAdministrador("8@gmail.com", "ed8", "8");
            List<Administrador> administrador = sf.obtieneAdministradores();

            if (administrador.size() != 6) 
                    fail("No se ha insertado el administrador");

            for (Administrador j : administrador) {
                    assertNotNull(j.getEmail());
                    assertNotNull(j.getNombre());
                    assertNotNull(j.getPassword());
                    
            }
            
        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se ha insertado el editorial al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }
    
    @Test
    public void testModificaAdministrador()
    {
        try
        {
            sf.modificaAdministrador("19@email.com","1","ed");

            Administrador a = sf.obtenerAdministrador("19@email.com");

           
            assert(a.getNombre().equals("ed"));
            assert(a.getPassword().equals("1"));
          


        } catch (Exception e) {
                    fail("TEST NO SUPERADO: No se ha modificado el administrador al producirse una excepcion: "
                                    + e.getLocalizedMessage());
        }
    }
    
    
    @Test
    public void testObtenerTodosAdministradores() 
    {
        try 
        {

                List<Administrador> administradores = sf.obtieneAdministradores();
                if (administradores.size() != 5) 
                        fail("No se han obtenido todas los administradores");
                
                for (Administrador a : administradores) {
                        assertNotNull(a.getEmail());
                        assertNotNull(a.getPassword());
                        assertNotNull(a.getNombre());
                       
                }
                
        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se han obtenido los administradores al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }
    
    
    @Test
    public void testEliminarAdministrador() 
    {
        try 
        {
            sf.borraAdministrador("19@email.com");
            
            List<Administrador> administrador = sf.obtieneAdministradores();
            if (administrador.size() != 4) 
                fail("No se ha eliminado el administrador");
            
            for (Administrador a : administrador) 
            {
                assertNotNull(a.getEmail());
                assertNotNull(a.getPassword());
                assertNotNull(a.getNombre());
                
            }

        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se ha eliminado el administrador al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }


     @Test
    public void testObtenerAdministradorEmail() 
    {
        try 
        {
            Administrador a = sf.obtenerAdministrador("19@email.com");

            assertNotNull(a.getEmail());
            assertNotNull(a.getPassword());
            assertNotNull(a.getNombre());
            

        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se han obtenido los administradores al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }



    
}
