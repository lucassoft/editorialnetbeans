package com.lucassoft.editorialjpa.entidades;

import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import javax.persistence.ManyToMany;

import javax.persistence.Version;

@Entity
@DiscriminatorValue("Coleccion")
public class Coleccion extends Obra{
    
     @Version
    private static final long serialVersionUID = 1L;
	
	private String descripcionColeccion;
        
        @ManyToMany// es para objeto, nose si vale para listas///////////////////////////////
	private List<SencillaColeccion> Obras;
	
	
	public String getDescripcionColeccion() {
		return descripcionColeccion;
	}
	public void setDescripcionColeccion(String descripcionColeccion) {
		this.descripcionColeccion = descripcionColeccion;
	}
	public List<SencillaColeccion> getObras() {
		return Obras;
	}
	public void setObras(List<SencillaColeccion> obras) {
		Obras = obras;
	}

}
