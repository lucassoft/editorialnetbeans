/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.SubGrupo;
import java.util.List;

/**
 *
 * @author Abraham
 */
public interface ISubGrupoDAO {
    
    public void nuevaSubGrupo(String nombre) throws DAOException;
    public SubGrupo obtenerSubGrupo(String nombre) throws DAOException;
    public List<SubGrupo> obtenerTodasSubGrupo() throws DAOException;
    public void eliminarSubGrupo(String nombre) throws DAOException; 
    public int modificaSubGrupo(String nombre) throws DAOException;
    public void finalizar() throws DAOException;
    
}
