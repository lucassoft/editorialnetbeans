/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.ColeccionJPADAOTEST;

import com.lucassoft.editorialjpa.entidades.Coleccion;

import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author franm
 */
public class ServicioColeccionTest {
    
    private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static ServicioColeccion sf=null;
    
        @BeforeClass
	public static void inicializarBD() throws Exception {

        try {

         sf= new ServicioColeccion();
       	 Properties pro = new Properties();
   		 pro.load(ColeccionJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            sf= new ServicioColeccion();
            
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	      sf= new ServicioColeccion();
	      //sf.elegirSistemaAlmacenamiento(5);
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        }
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
        
        //Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
	}
        
        //Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
        
        //Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	@After
	public void tearDown() throws Exception {
	}
	
        @Test
	public void testNuevoColeccion()
        {	
            try
            {
                String[] array_obrascoleccion;
                array_obrascoleccion = new String[1];
                array_obrascoleccion[0]="446-55";

                sf.nuevaColeccion("44-556","1984", "Geroge Orwell", "Español", true, "1","tipoobra1","Politica","prueba","Valla Valla, Aqui no hay playa",array_obrascoleccion);

                List<Coleccion> coleccion = sf.obtenerTodasColecciones();
                if (coleccion.size() != 6) 
                        fail("No se ha insertado la Sencilla Coleccion");

                for (Coleccion t : coleccion) 
                {
                    assertNotNull(t.getISBN());
                    assertNotNull(t.getNombre());
                    assertNotNull(t.getAutor());
                    assertNotNull(t.getIdioma());
                    assertNotNull(t.getDerechos());
                    assertNotNull(t.getEditorial());
                    assertNotNull(t.getTipo());
                    assertNotNull(t.getSubtipoObra());
                    assertNotNull(t.getDescripcion());
                    assertNotNull(t.getDescripcionColeccion());
                            
                }

            } catch (Exception e) {
                    fail("TEST NO SUPERADO: No se ha insertado la Coleccion al producirse una excepcion: " + e.getLocalizedMessage());
            }			
        }
        
        
         @Test   
		public void testEliminarColeccion()
                {	
                    try 
                    {
                        sf.eliminaColeccion("44-554");
                        List<Coleccion> coleccion = sf.obtenerTodasColecciones();
                        
                        if (coleccion.size() != 4) 
                                fail("No se ha eliminado la Coleccion");
                        
                        //if(sf.obtenerColeccion("44-554")!=null)
                        //    fail("TEST NO SUPERADO: No se ha eliminado la Coleccion seleccionada");

                    } catch (Exception e) {
                            fail("TEST NO SUPERADO: No se ha eliminado la Coleccion al producirse una excepcion: "
                                            + e.getLocalizedMessage());
                    }
		}
                
         @Test
		public void testObtenerTodasColeccion(){
			
			try {

				List<Coleccion> coleccion = sf.obtenerTodasColecciones();
				if (coleccion.size() != 5) {
					fail("No se han obtenido todas la Coleccion");
				}
				for (Coleccion t : coleccion) {
					
                                    //assertNotNull(t.getID());
                                        assertNotNull(t.getISBN());
                                        assertNotNull(t.getNombre());
                                        assertNotNull(t.getAutor());
                                        assertNotNull(t.getIdioma());
                                        assertNotNull(t.getDerechos());
                                        assertNotNull(t.getEditorial());
                                        assertNotNull(t.getTipo());
                                        assertNotNull(t.getSubtipoObra());
                                        assertNotNull(t.getDescripcion());
                                        assertNotNull(t.getDescripcionColeccion());
					
				}
			} catch (Exception e) {
				fail("TEST NO SUPERADO: No se han obtenido la Coleccion al producirse una excepcion: "
						+ e.getLocalizedMessage());
			}
			
			
		}
                
                
             @Test
                public void testModificarColeccion(){
                    try
                    {
                        
                        sf.modificaColeccion("44-521","Alicia en el pais de..", "Lewis Carroll", "Español", true, "2","tipoobra2","Fantasia","prueba","prueba");
                        
                        Coleccion c = sf.obtenerColeccion("44-521");
                        				
                        assertNotNull(c.getISBN());
                        assertNotNull(c.getNombre());
                        assertNotNull(c.getAutor());
                        assertNotNull(c.getIdioma());
                        assertNotNull(c.getDerechos());
                        assertNotNull(c.getEditorial());
                        assertNotNull(c.getTipo());
                        assertNotNull(c.getSubtipoObra());
                        assertNotNull(c.getDescripcion());
                        assertNotNull(c.getDescripcionColeccion());
                        
                    } catch (Exception e) {
			fail("TEST NO SUPERADO: No se ha modificado la Coleccion al producirse una excepcion: "
					+ e.getLocalizedMessage());
                }
	}
}
