/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;

import com.lucassoft.editorialjpa.datos.ColeccionJPADAO;
import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.EditorialJPADAO;
import com.lucassoft.editorialjpa.datos.SencillaColeccionJPADAO;
import com.lucassoft.editorialjpa.datos.TipoObraJPADAO;
import com.lucassoft.editorialjpa.entidades.ClienteIndividual;
import com.lucassoft.editorialjpa.entidades.Coleccion;
import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.SencillaColeccion;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import com.lucassoft.editorialjpa.entidades.TipoObra;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class ServicioColeccion {
    
    ColeccionJPADAO idao=null;
    EditorialJPADAO idaoe=null;
    TipoObraJPADAO idaoto=null;
    SencillaColeccionJPADAO idaosc=null;
    
    public ServicioColeccion() throws DAOException
	{
		idao=new ColeccionJPADAO();
                idaoe=new EditorialJPADAO();
                idaoto=new TipoObraJPADAO();
                idaosc=new SencillaColeccionJPADAO();
	}
    
    
    
    public void nuevaColeccion(String isbn, String nombre, String autor, String idioma, boolean derechos, String cif, String tipoobra, String subtipoobra, String descripcion,String descripcionColeccion , String[] array_obras) throws ServicioException, DAOException
	{
            Coleccion col = new Coleccion();
            
            List<Coleccion> colecciones = idao.obtenerTodasColecciones();
            if(colecciones.isEmpty())
                col.setId(0);
            else
            {
                long id= colecciones.get(colecciones.size()-1).getId()+1;
                col.setId(id);
            }
            
            col.setISBN(isbn);
            col.setNombre(nombre);
            col.setAutor(autor);
            col.setIdioma(idioma);
            col.setDerechos(derechos);
            col.setSubtipoObra(subtipoobra);
            col.setDescripcion(descripcion);
            

            Editorial e = idaoe.obtenerEditorial(cif);
            
                if(e==null)
                    throw new ServicioException("No existe ningúna editorial con ese cif");
            
            col.setEditorial(e);
            
            TipoObra to = idaoto.obtenerTipoObra(tipoobra);
            
                if(to==null)
                    throw new ServicioException("No existe ningún tipo de obra con ese nombre");
            
            col.setTipo(to);
            col.setDescripcionColeccion(descripcionColeccion);
            
            
            
           List <SencillaColeccion> obras= new ArrayList();
            
           for (int i=0;i<array_obras.length;i++)
           {
               SencillaColeccion sc=idaosc.obtenerSencillaColeccion(array_obras[i]);
               if (sc==null)
               {
                 throw new ServicioException("No existe una SencillaColeccion con ese ISBN");        
               }
               else
               {
                 obras.add(sc);  
               }
               
           }
                       
            col.setObras(obras);
	 	
            Coleccion colPrueba=idao.obtenerColeccion(isbn);
            
            if(colPrueba!=null)
                throw new ServicioException("Ya existe una coleccion con ese ISBN");
        
            idao.nuevaColeccion(col);
	}
    
    
    public Coleccion obtenerColeccion(String isbn) throws DAOException, ServicioException
	{ 
            Coleccion col= idao.obtenerColeccion(isbn);
	        
	    if(col==null){                
                throw new ServicioException("No existe ningúna coleccion con ese isbn");                  
            }
                      
	        
	    return col;
	}
    
    
    public List<Coleccion> obtenerTodasColecciones() throws DAOException
	{
            List<Coleccion> colecciones= idao.obtenerTodasColecciones();

            if(colecciones==null)
                colecciones=new ArrayList<Coleccion>();

            return colecciones;
	}
    
    
    public void eliminaColeccion(String isbn) throws ServicioException, DAOException
	{
            Coleccion col= idao.obtenerColeccion(isbn);
	        
	    if(col==null)
                throw new ServicioException("No existe ningúna colección con ese isbn");
            
            idao.eliminarColeccion(isbn); 
	}
    
    
    
    public void modificaColeccion(String isbn, String nombre, String autor, String idioma, boolean derechos, String cif, String tipoobra, String subtipoobra, String descripcion,String descripcionColeccion) throws ServicioException, DAOException
	{
            
            Coleccion col = new Coleccion();
		
            //col.setISBN(isbn);
            col.setNombre(nombre);
            col.setAutor(autor);
            col.setIdioma(idioma);
            col.setDerechos(derechos);
            col.setSubtipoObra(subtipoobra);
            col.setDescripcion(descripcion);
            col.setDescripcionColeccion(descripcion);
            Editorial e = idaoe.obtenerEditorial(cif);
            
                if(e==null)
                    throw new ServicioException("No existe ningúna editorial con ese cif");
            
            col.setEditorial(e);
            
            TipoObra to = idaoto.obtenerTipoObra(tipoobra);
            
                if(to==null)
                    throw new ServicioException("No existe ningún tipo de obra con ese nombre");
            
            col.setTipo(to);
            
                
            Coleccion colPrueba=idao.obtenerColeccion(isbn);
                    
            if(colPrueba==null)
                throw new ServicioException("No existe una colección con ese isbn");
            
            int afectadas = idao.modificaColeccion(isbn,col);
        
            if (afectadas==0)
        	throw new ServicioException("No se ha modificado ninguna colección.");
	}
    
     public void coleccionobranueva( String isbnColeccion, String isbnSencillaColeccion) throws DAOException, ServicioException
        {
            Coleccion coleccioni=idao.obtenerColeccion(isbnColeccion);
            
            if(coleccioni==null)
                throw new ServicioException("No existe un cliente individual con ese email");
            
            SencillaColeccion sencillai = idaosc.obtenerSencillaColeccion(isbnSencillaColeccion);
            
            if(sencillai==null)
            {
                throw new ServicioException("No existe una SencillaNormal con ese isbn");
            }
            
            List<SencillaColeccion>sencillacoleccion = coleccioni.getObras();
            
            int cuenta = 0;
            
            for(SencillaColeccion comprueba:sencillacoleccion)
                if(comprueba.getISBN().equals(sencillai.getISBN()))
                    {
                        cuenta++;
                        throw new ServicioException("Ya existe una SencillaNormal con ese isbn para ese cliete");
                    }
            
            if(cuenta==0)
                sencillacoleccion.add(sencillai);
                
        }
        
        public void coleccionobraborrar( String isbnColeccion, String isbnSencillaColeccion) throws DAOException, ServicioException
        {
             Coleccion coleccioni=idao.obtenerColeccion(isbnColeccion);
            
            if(coleccioni==null)
                throw new ServicioException("No existe un cliente individual con ese email");
            
            SencillaColeccion sencillai = idaosc.obtenerSencillaColeccion(isbnSencillaColeccion);
            
            if(sencillai==null)
            {
                throw new ServicioException("No existe una SencillaColeccion con ese isbn");
            }
            
            List<SencillaColeccion>sencillacoleccion = coleccioni.getObras();
            
            int cuenta = 0;
            
            for(SencillaColeccion comprueba:sencillacoleccion)
                if(comprueba.getISBN().equals(sencillai.getISBN()))
                {
                    sencillacoleccion.remove(sencillai);
                }
                else
                {
                    throw new ServicioException("No existe una SencillaColeccion con ese isbn");
                }
            
            
        }
    
    
    
    
}
