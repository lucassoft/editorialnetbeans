package com.lucassoft.editorialjpa.entidades;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Version;

@Entity
@DiscriminatorValue("SencillaNormal")

public class SencillaNormal extends Sencilla {
 @Version
    private static final long serialVersionUID = 1L;
 
       
	private float precioIndividual;

	public float getPrecioIndividual() {
		return precioIndividual;
	}

	public void setPrecioIndividual(float precioIndividual) {
		this.precioIndividual = precioIndividual;
	}
}
