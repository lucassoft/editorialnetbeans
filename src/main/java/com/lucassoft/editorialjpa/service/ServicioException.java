package com.lucassoft.editorialjpa.service;

public class ServicioException extends Exception 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ServicioException(String msg, Exception e) 
	{
        super(msg,e);
        }
	public ServicioException(String msg) 
	{
        super(msg);
    }
}