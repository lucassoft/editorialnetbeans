package com.lucassoft.editorialjpa.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import com.lucassoft.editorialjpa.datos.EditorialJPADAOTEST;
import com.lucassoft.editorialjpa.entidades.Editorial;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import com.mysql.jdbc.Connection;

public class ServicioEditorialTest 
{

    private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ServicioEditorial sf=null;
	
    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            sf= new ServicioEditorial();
            Properties pro = new Properties();
            pro.load(EditorialJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
                    bd=pro.getProperty("bdJDBC");
                    login=pro.getProperty("loginJDBC");
                    password=pro.getProperty("passwordJDBC");
                    url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                       .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);


            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            sf= new ServicioEditorial();

            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("test-DAO-dataset.xml"));

        } catch (InstantiationException ex) {
                System.out.println("Hubo un problema al iniciar la base de datos del test: ");
                ex.printStackTrace();
        } catch (IllegalAccessException iae) {
                System.out.println("Hubo un problema al acceder a la base de datos. del test");
                iae.printStackTrace();
        } catch (SQLException sqle) {
                System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
                sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
                System.out.println("No se encuentra el driver JDBC."+cnfe);
                cnfe.printStackTrace();
        } catch (IOException ioe) {
                System.out.println("Error al accerder al archivo de configuración: "+ioe);
                ioe.printStackTrace();
        } catch (Exception ex) {
                System.out.println("Error inesperado al iniciar los test");
                ex.printStackTrace();
        }
   }


    @AfterClass
    public static  void finalizar() throws Exception{
            if(connection!=null){
                    connection.close();
            }
    }


    @Before
    public void setUp() throws Exception{
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }


    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void testNuevaEditorial()
    {
        try 
        {
            sf.nuevaEditorial("8@gmail.com", "ed8", "8", "8", "cc8", "pais", "rs8", "8", "28");
            List<Editorial> editorial = sf.obtieneEditoriales();

            if (editorial.size() != 6) 
                    fail("No se ha insertado la editorial");

            for (Editorial j : editorial) {
                    assertNotNull(j.getCif());
                    assertNotNull(j.getRazonSocial());
                    assertNotNull(j.getTelefono1());
                    assertNotNull(j.getTelefono2());
                    assertNotNull(j.getCuentaCorriente());
                    assertNotNull(j.getPais());
            }
            
        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se han insertado la editorial al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }

    @Test(expected = ServicioException.class)
    public void testNuevaEditorialConEmailRepetido() throws Exception 
    {
        sf.nuevaEditorial("1@email.com","6","ed6","6", "rs6", "6", "62", "cc6", "país");
    }

    @Test(expected = ServicioException.class)
    public void testNuevaEditorialConCifRepetido() throws Exception 
    {
        sf.nuevaEditorial("1@email.com","1","ed1","1", "rs1", "1", "12", "cc1", "pais");
    }

    @Test
    public void testModificarEditorial()
    {
        try
        {
            sf.modificaEditorial("1","111","edUno", "rs111", "111", "1112", "cc111", "país1");

            Editorial e = sf.obtenerEditorial("1");

            assert(e.getPassword().equals("111"));
            assert(e.getNombre().equals("edUno"));
            assert(e.getRazonSocial().equals("rs111"));
            assert(e.getTelefono1().equals("111"));
            assert(e.getTelefono2().equals("1112"));
            assert(e.getCuentaCorriente().equals("cc111"));
            assert(e.getPais().equals("país1"));


        } catch (Exception e) {
                    fail("TEST NO SUPERADO: No se ha modificado la editorial al producirse una excepcion: "
                                    + e.getLocalizedMessage());
        }
    }

    @Test
    public void testObtenerEditorial() 
    {
        try 
        {
            Editorial e = sf.obtenerEditorial("1");

            assertNotNull(e.getEmail());
            assertNotNull(e.getPassword());
            assertNotNull(e.getNombre());
            assertNotNull(e.getCif());
            assertNotNull(e.getRazonSocial());
            assertNotNull(e.getTelefono1());
            assertNotNull(e.getTelefono2());
            assertNotNull(e.getCuentaCorriente());
            assertNotNull(e.getPais());

        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se han obtenido las editoriales al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }



    @Test
    public void testObtenerTodasEditoriales() 
    {
        try 
        {

                List<Editorial> editoriales = sf.obtieneEditoriales();
                if (editoriales.size() != 5) 
                        fail("No se han obtenido todas las editoriales");
                
                for (Editorial e : editoriales) {
                        assertNotNull(e.getEmail());
                        assertNotNull(e.getPassword());
                        assertNotNull(e.getNombre());
                        assertNotNull(e.getCif());
                        assertNotNull(e.getRazonSocial());
                        assertNotNull(e.getTelefono1());
                        assertNotNull(e.getTelefono2());
                        assertNotNull(e.getCuentaCorriente());
                        assertNotNull(e.getPais());
                }
                
        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se han obtenido las editoriales al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }



    @Test
    public void testEliminarEditorial() 
    {
        try 
        {
            sf.borraEditorial("1");
            
            List<Editorial> editorial = sf.obtieneEditoriales();
            if (editorial.size() != 4) 
                fail("No se ha eliminado la editorial");
            
            for (Editorial e : editorial) 
            {
                assertNotNull(e.getEmail());
                assertNotNull(e.getPassword());
                assertNotNull(e.getNombre());
                assertNotNull(e.getCif());
                assertNotNull(e.getRazonSocial());
                assertNotNull(e.getTelefono1());
                assertNotNull(e.getTelefono2());
                assertNotNull(e.getCuentaCorriente());
                assertNotNull(e.getPais());
            }

        } catch (Exception e) {
                fail("TEST NO SUPERADO: No se ha eliminado la editorial al producirse una excepcion: "
                                + e.getLocalizedMessage());
        }
    }
}
