package com.lucassoft.editorialjpa.datos;

import java.util.List;
import com.lucassoft.editorialjpa.entidades.Editorial;

public interface IEditorialDAO 
{
	public void nuevaEditorial(Editorial ed) throws DAOException;
        public void finalizar() throws DAOException;
	public Editorial obtenerEditorial(String cif) throws DAOException;
	public Editorial obtenerEditorialEmail(String email) throws DAOException;
	public int eliminarEditorial(String cif) throws DAOException;
	public List<Editorial> obtenerTodasEditoriales() throws DAOException;
	public int modificaEditorial(String cif, Editorial e) throws DAOException;
}
