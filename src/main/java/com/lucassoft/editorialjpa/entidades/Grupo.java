/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.entidades;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Version;

/**
 *
 * @author kiko
 */
@Entity
@DiscriminatorValue("Grupo")
public class Grupo extends Cliente implements Serializable{
    
    @Version
    private static final long serialVersionUID = 1L;
     
        protected String nombreResponsable;
        protected String cargoResponsable;

        public String getNombreResponsable() {
            return nombreResponsable;
        }

        public void setNombreResponsable(String nombreResponsable) {
            this.nombreResponsable = nombreResponsable;
        }

        public String getCargoResponsable() {
            return cargoResponsable;
        }

        public void setCargoResponsable(String cargoResponsable) {
            this.cargoResponsable = cargoResponsable;
        }




}
