package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import com.lucassoft.editorialjpa.entidades.SencillaNormal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

public class SencillaNormalJPADAO implements ISencillaNormalDAO
{
    EntityManagerFactory emf=null;
    EditorialJPADAO e_idao = null;
    
    public SencillaNormalJPADAO() throws DAOException 
    {
        try
        {
            emf= Emf.getInstance().getEmf();
            e_idao = new EditorialJPADAO();
            
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        } 
    }

    public void nuevaSencillaNormal(SencillaNormal sn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        try 
        {
            em.persist(sn);
            em.getTransaction().commit();
            
        } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar la obra sencilla", ex);
        } finally {
            em.close();  
        } 
    }

    public SencillaNormal obtenerSencillaNormal(String isbn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            SencillaNormal sn = null;
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();
                
            Query query = em.createQuery("SELECT sn FROM SencillaNormal sn WHERE sn.ISBN=:isbn");
            query.setParameter("isbn",isbn);
            List sencilla=query.getResultList();
            List<SencillaNormal> sencillaNormal=(List<SencillaNormal>) sencilla;
            
            if(sencillaNormal.isEmpty())
                return null;
            
            return sencillaNormal.get(0);
                      
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener la obra sencilla", ex);
        } finally {
            em.close(); 
        } 
    }

    public List<SencillaNormal> obtenerTodasSencillas() throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT sn FROM SencillaNormal sn ");
            
            List sencillas = query.getResultList();
            return sencillas;
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener las obras sencillas", ex);
        } finally {
            em.close();
        }
    }
    
    public List<SencillaNormal> obtenerTodasSencillasEditorial(String cif) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        
        try
        {
            Editorial e = e_idao.obtenerEditorial(cif);
            long id_editorial = e.getId();
            
            em.getTransaction().begin();
            em.flush();
            em.getTransaction().commit();

            Query query = em.createQuery("SELECT sn FROM SencillaNormal sn WHERE sn.editorial.id=:id_editorial");
            query.setParameter("id_editorial",id_editorial);
         
            List<SencillaNormal> sencillas = query.getResultList();
            
            return sencillas;
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener las obras sencillas", ex);
        } finally {
            em.close();
        }
    }

    public int eliminarSencillaNormal(String isbn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
                
        try
        {
            SencillaNormal identSN=this.obtenerSencillaNormal(isbn);
            long id = identSN.getId();
            
            em.getTransaction().begin();
                SencillaNormal sn= em.find(SencillaNormal.class,id);
            em.remove(sn);
            em.getTransaction().commit();

            return 1;
            
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al eliminar la obra sencilla", ex);
        } finally {
            em.close();
        }
    }
    
    public int modificaSencillaNormal(String isbn, SencillaNormal sn) throws DAOException 
    {
        EntityManager em = emf.createEntityManager();
        try
        {
            em.getTransaction().begin();
            em.flush();

            SencillaNormal identSN=this.obtenerSencillaNormal(isbn);
            long id = identSN.getId();

            SencillaNormal sencilla = em.find(SencillaNormal.class,id);

            sencilla.setNombre(sn.getNombre());
            sencilla.setAutor(sn.getAutor());
            sencilla.setIdioma(sn.getIdioma());
            sencilla.setDerechos(sn.getDerechos());
            sencilla.setEditorial(sn.getEditorial());
            sencilla.setTipo(sn.getTipo());
            sencilla.setSubtipoObra(sn.getSubtipoObra());
            sencilla.setDescripcion(sn.getDescripcion());
            sencilla.setPrecioIndividual(sn.getPrecioIndividual());

            em.persist(sencilla);
            em.getTransaction().commit();

            return 1;
                                
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DAOException("Ha habido un error al obtener la obra sencilla", ex);  
        } finally {
            em.close(); 
        }
    } 
    
    
    public void finalizar() throws DAOException 
    {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }
}
