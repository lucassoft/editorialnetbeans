package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.Editorial;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class EditorialJPADAOTEST{
	
	//Fíjate son variables static de clase
	private static String bd = null;
	private static String login = null;
	private static String password = null;
	private static String url =null;
	
	
	private static IDatabaseConnection connection;
	private static IDataSet dataset;
	private static IEditorialDAO idao=null;
	
	
	//Este método se ejucuta UNA vez, justo antes de empezar las pruebas
	@BeforeClass
	public static void inicializarBD() throws Exception {
		

        try {
       	 //Ojo esta es una clase de test obtendrá la configuración
        //del .properties de src/TEST/resources NO de src/main/resources
       	 Properties pro = new Properties();
   		 pro.load(EditorialJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
   		 bd=pro.getProperty("bdJDBC");
   		 login=pro.getProperty("loginJDBC");
   		 password=pro.getProperty("passwordJDBC");
   		 url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
 	               .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);
            

            // establecemos en dbunit que se ttrata de una bd de pysql, si no da un warning
            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

       	 
	         FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
	         flatXmlDataSetBuilder.setColumnSensing(true);
	         dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
	               .getContextClassLoader()
	               .getResourceAsStream("test-DAO-dataset.xml"));
	         //test-dao-dataset debe de estar en src/test/resources
	       
            //Al ejecutar desde aquí la instucción del new 
	         //en el constructor coge la configuración de la bd de los recursos de test
	         idao= new EditorialJPADAO();
            

        } catch (InstantiationException ex) {
        	
       	 	System.out.println("Hubo un problema al iniciar la base de datos del test: ");
       	 	ex.printStackTrace();
        } catch (IllegalAccessException iae) {
       	 
        	System.out.println("Hubo un problema al acceder a la base de datos. del test");
        	iae.printStackTrace();
        } catch (SQLException sqle) {
        	System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
        	sqle.printStackTrace();
            
        } catch (ClassNotFoundException cnfe) {
        	System.out.println("No se encuentra el driver JDBC."+cnfe);
        	cnfe.printStackTrace();
        } catch (IOException ioe) {
        	System.out.println("Error al accerder al archivo de configuración: "+ioe);
        	ioe.printStackTrace();
		}
        catch (Exception ex) {
        	
        	System.out.println("Error inesperado al iniciar los test");
        	ex.printStackTrace();
	      }
	}
	
	//Este método se ejecuta Una vez al finalizar todos los test
	@AfterClass
	public static  void finalizar() throws Exception{
		if(connection!=null){
			connection.close();
		}
                //Esto es nuevo hay que cerrar la factoria de sesiones
		idao.finalizar();
	}
	
	
	//Este método se ejecuta tantas veces como test haya, es decir, 
	//Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
	@Before
	public void setUp() throws Exception{
		DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);
		
	}
	
	//Este método se ejecuta Después de ejeucutar cada uno de los test
	//En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar
	
	
	
	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void testObtenerTodasEditoriales() {
		try{
			List<Editorial> editoriales= idao.obtenerTodasEditoriales();
			
			assert(editoriales.size()==5);
			
			for(Editorial e :editoriales)
				assertNotNull(e.getCif());
			
			Editorial e=editoriales.get(0);
			
			assert(e.getEmail().equals("1@email.com"));
			assert(e.getPassword().equals("1"));
			assert(e.getNombre().equals("ed1"));
			assert(e.getCif().equals("1"));
			assert(e.getRazonSocial().equals("rs1"));
			assert(e.getTelefono1().equals("1"));
			assert(e.getTelefono2().equals("12"));
			assert(e.getCuentaCorriente().equals("cc1"));
			assert(e.getPais().equals("pais"));
			
			Editorial edi = null;
			
			for(Editorial e2 : editoriales)
				if(e2.getCif().equals("1"))
					edi=e;
			
			assertNotNull(edi);
			assert(edi.getNombre().equals("ed1"));
			assertNotNull(edi.getEmail());
			
			
			}catch(Exception e){
				fail("TEST NO SUPERADO: No se ha podido obtener la lista vacia  de editoriales al producirse una excepcion: "+e.getLocalizedMessage());
			}
	}
	
	
	@Test
	public void testNuevaEditorial() 
        {
            try
            {
                Editorial e1= new Editorial();
                List<Editorial>editoriales = idao.obtenerTodasEditoriales();
                long id= editoriales.get(editoriales.size()-1).getId()+1;
                
                e1.setId(id); 
                e1.setEmail("prueba@email.com");
                e1.setPassword("prueba");
                e1.setNombre("EditorialKiko");
                e1.setCif("11111111L");
                e1.setRazonSocial("kiko");
                e1.setTelefono1("619154672");
                e1.setTelefono2("965479079");
                e1.setCuentaCorriente("12345678901234567890");
                e1.setPais("España");

                idao.nuevaEditorial(e1);

                editoriales=idao.obtenerTodasEditoriales();

                Editorial e2=null;
                for(Editorial e :editoriales)
                        if(e.getCif().equals("11111111L"))
                                e2=e;

                assertNotNull(e2);
                assert(e2.getEmail().equals("prueba@email.com"));
                assert(e2.getPassword().equals("prueba"));
                assert(e2.getNombre().equals("EditorialKiko"));
                assert(e2.getCif().equals("11111111L"));
                assert(e2.getRazonSocial().equals("kiko"));
                assert(e2.getTelefono1().equals("619154672"));
                assert(e2.getTelefono2().equals("965479079"));
                assert(e2.getCuentaCorriente().equals("12345678901234567890"));
                assert(e2.getPais().equals("España"));

            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha insertado la editorial al producirse una excepcion: "+e.getLocalizedMessage());
            }
	}

	
	@Test
	public void testObtenerEditorial() {
		
		try
		{
			Editorial e=idao.obtenerEditorial("1");
			
			if(e.getEmail()==null)
				fail("No se ha obtenido la editorial apropiadamente, la editorial no tiene email");
			
			if(!(e.getPassword().equals("1")))
				fail("No se ha obtenido la editorial apropiadamente, la contraseña no es la correcta");
			
			if(!(e.getNombre().equals("ed1")))
				fail("No se ha obtenido la editorial apropiadamente, el nombre no es el correcto");
			
			if(e.getCif()==null)
				fail("No se ha obtenido la editorial apropiadamente, la editorial no tiene cif");
			
			if(!(e.getRazonSocial().equals("rs1")))
				fail("No se ha obtenido la editorial apropiadamente, la editorial no tiene razón social");
			
			if(!(e.getTelefono1().equals("1")))
				fail("No se ha obtenido la editorial apropiadamente, el telefono1 no es el correcto");
			
			if(!(e.getTelefono2().equals("12")))
				fail("No se ha obtenido la editorial apropiadamente, el telefono2 no es el correcto");
			
			if(!(e.getCuentaCorriente().equals("cc1")))
				fail("No se ha obtenido la editorial apropiadamente, la cuenta corriente no es la correcta");
			
			if(!(e.getPais().equals("pais")))
				fail("No se ha obtenido la editorial apropiadamente, el país no es el correcto");
			
			
		}catch(Exception e){
			fail("TEST NO SUPERADO: No se ha podido obtener la lista de editoriales al producirse una excepcion: "+e.getLocalizedMessage());
                }
	}
        
        
    @Test
    public void testModificaEditorial() 
    {
        try
        {
            Editorial e = new Editorial();
            
            e.setPassword("111");
            e.setNombre("edUno");
            e.setRazonSocial("rs111");
            e.setTelefono1("111");
            e.setTelefono2("1112");
            e.setCuentaCorriente("cc111");
            e.setPais("país1");
            
            idao.modificaEditorial("1",e);
                
            Editorial edi = idao.obtenerEditorial("1");
                
            assert(edi.getPassword().equals("111"));
            assert(edi.getNombre().equals("edUno"));
            assert(edi.getRazonSocial().equals("rs111"));
            assert(edi.getTelefono1().equals("111"));
            assert(edi.getTelefono2().equals("1112"));
            assert(edi.getCuentaCorriente().equals("cc111"));
            assert(edi.getPais().equals("país1"));

        }catch(Exception e){
            fail("TEST NO SUPERADO: No se ha podido modificar el tipo de obra al producirse una excepcion: "+e.getLocalizedMessage());
        }
    }
	

	@Test
	public void testEliminaEditorial() 
        {
            try
            {
                List<Editorial> editoriales=null;
                idao.eliminarEditorial("1");
                editoriales=idao.obtenerTodasEditoriales();
                
                if(!(editoriales.size()==4))
                    fail("TEST NO SUPERADO: No se ha eliminado la editorial");
                
                if(idao.obtenerEditorial("1")!=null)
                    fail("TEST NO SUPERADO: No se ha eliminado la editorial seleccionada");
   
            }catch(Exception e){
                    fail("TEST NO SUPERADO: No se ha podido obtener la lista de editoriales al producirse una excepcion: "+e.getLocalizedMessage());
            }
	}
}
