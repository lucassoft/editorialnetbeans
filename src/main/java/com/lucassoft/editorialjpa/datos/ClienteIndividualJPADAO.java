/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.datos;

import com.lucassoft.editorialjpa.entidades.ClienteIndividual;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author Antonio
 */
public class ClienteIndividualJPADAO implements IClienteIndividualDAO{
        EntityManagerFactory emf=null;

    
    
    public ClienteIndividualJPADAO() throws DAOException {
        try{
            //Creamos la factoria
        emf= Emf.getInstance().getEmf();
        
        }catch(Exception e){
            throw  new DAOException("Error al iniciar la factoría de sesiones ", e);
        }
        
    }
    
    
    public void nuevoClienteIndividual(ClienteIndividual u) throws DAOException {
        

        // Creamos el entity Manager
         
        EntityManager em = emf.createEntityManager();

        // Marcamos el comienzo de la transacción
        em.getTransaction().begin();
        try {
           

            // Lo añadimos a la BD
            em.persist(u);
           
            //Hacemos commit es decir guardamos los cambios
            em.getTransaction().commit();
        } catch (Exception ex) {
            //Si ha habido cualquier problema podemos volver atrás la transacción
            //esto será util cuando haganos varias cosas en una única transaccion
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al guardar el cliente individual", ex);
            
            
        } finally {
            em.close();
            
        }
    }
    
    public void eliminarClienteIndividual(long id) throws DAOException {
          
     
       EntityManager em = emf.createEntityManager();
         em.getTransaction().begin();
         try{
         //Mediante find obtenemos un objeto a partir de su clave primaria
         ClienteIndividual e=em.find(ClienteIndividual.class,id);
         //Así borramos el jugador
         em.remove(e);
         em.getTransaction().commit();
         
         } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al eliminar el cliente individual", ex);
        } finally {
            em.close();
            
        }
         
    }
    
     public ClienteIndividual obtenerClienteIndividualEmail(String email) throws DAOException {
         EntityManager em = emf.createEntityManager();
         try{
                ClienteIndividual e = null;
                em.getTransaction().begin();
                em.flush();
                em.getTransaction().commit();
                
                Query query = em.createQuery("SELECT e FROM ClienteIndividual e WHERE e.email=:email");
                query.setParameter("email",email);
                List clienteindividual=query.getResultList();
                List<ClienteIndividual> edit=(List<ClienteIndividual>) clienteindividual;
                if(edit.size()==0){
                    return null;
                }
                return edit.get(0);
                    
                
                
                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener el cliente individual", ex);
                
        } finally {
            em.close(); 
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ClienteIndividual> obtenerTodosClienteIndividual() throws DAOException {
      EntityManager em = emf.createEntityManager();
      try{
      em.getTransaction().begin();
      em.flush();
      em.getTransaction().commit();
      //Para realizar consultas usaremos el lenguaje JPQL, muy parecido a HQL y a SQL
      //Al igual que HQL recuerda que no hacemos consultas sobre tablas sino sobre objetos
      Query query = em.createQuery("SELECT e FROM ClienteIndividual e ");
      //En caso de necesitar parámetros los ontroducimos así en la consulta sería con : igual que HQL
      //query.setParameter("parametro",valor);
      List ci = query.getResultList();
      return ci;
      } catch (Exception ex) {
           em.getTransaction().rollback();
           throw new DAOException("Ha habido un error al obtener los clientes individuales", ex);
        } finally {
            em.close();
            
        }
    }
    
    public void finalizar() throws DAOException {
        //Hay que cerrar el Entity Manager Factory
        emf.close();
    }

    public int modificaClienteIndividual(String email, ClienteIndividual e) throws DAOException {
         EntityManager em = emf.createEntityManager();
         try{
                em.getTransaction().begin();
                em.flush();
                
                ClienteIndividual identEdi=this.obtenerClienteIndividualEmail(email);
                long id = identEdi.getId();
                
                ClienteIndividual cli = em.find(ClienteIndividual.class,id);
                
                cli.setNombre(e.getNombre());
                cli.setPassword(e.getPassword());
                cli.setEmail(e.getEmail());
                cli.setPais(e.getPais());
                cli.setProvincia(e.getProvincia());
                cli.setLocalidad(e.getLocalidad());
                cli.setCodigopostal(e.getCodigopostal());
                cli.setDireccion(e.getDireccion());
                cli.setFechaNacimiento(e.getFechaNacimiento());
                //Faltan los de los subgrupos y las obras individuales
                
                em.persist(cli);
                em.getTransaction().commit();
                
                return 1;
                                
         } catch (Exception ex) {
                em.getTransaction().rollback();
                throw new DAOException("Ha habido un error al obtener el cliente individual", ex);
                
        } finally {
            em.close(); 
        }
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    



}
   
