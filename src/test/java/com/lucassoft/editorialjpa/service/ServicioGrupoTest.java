/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lucassoft.editorialjpa.service;


import com.lucassoft.editorialjpa.datos.DAOException;
import com.lucassoft.editorialjpa.datos.GrupoJPADAOTEST;
import com.lucassoft.editorialjpa.entidades.Grupo;
import com.mysql.jdbc.Connection;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Kraftwerk
 */
public class ServicioGrupoTest {
    
    private static String bd = null;
    private static String login = null;
    private static String password = null;
    private static String url =null;
    private static IDatabaseConnection connection;
    private static IDataSet dataset;
    private static ServicioGrupo s=null;
	
    @BeforeClass
    public static void inicializarBD() throws Exception 
    {
        try 
        {
            s= new ServicioGrupo();
            Properties pro = new Properties();
            pro.load(GrupoJPADAOTEST.class.getResourceAsStream("/configuracion.properties"));
                    bd=pro.getProperty("bdJDBC");
                    login=pro.getProperty("loginJDBC");
                    password=pro.getProperty("passwordJDBC");
                    url= "jdbc:mysql://localhost/" + bd;

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection jdbcConnection = (Connection) DriverManager
                       .getConnection(url,login,password);
            connection = new DatabaseConnection(jdbcConnection);


            DatabaseConfig dbConfig = connection.getConfig();
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());

            s= new ServicioGrupo();

            FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
            flatXmlDataSetBuilder.setColumnSensing(true);
            dataset = flatXmlDataSetBuilder.build(Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("test-DAO-dataset.xml"));

        } catch (InstantiationException ex) {
                System.out.println("Hubo un problema al iniciar la base de datos del test: ");
                ex.printStackTrace();
        } catch (IllegalAccessException iae) {
                System.out.println("Hubo un problema al acceder a la base de datos. del test");
                iae.printStackTrace();
        } catch (SQLException sqle) {
                System.out.println("Hubo un problema al intentar conectarse con la base de datos "+sqle);
                sqle.printStackTrace();
        } catch (ClassNotFoundException cnfe) {
                System.out.println("No se encuentra el driver JDBC."+cnfe);
                cnfe.printStackTrace();
        } catch (IOException ioe) {
                System.out.println("Error al accerder al archivo de configuración: "+ioe);
                ioe.printStackTrace();
        } catch (Exception ex) {
                System.out.println("Error inesperado al iniciar los test");
                ex.printStackTrace();
        }
   }

    
    //Este método se ejecuta Una vez al finalizar todos los test
    @AfterClass
    public static  void finalizar() throws Exception{
            if(connection!=null){
                    connection.close();
            }
    }


    //Este método se ejecuta tantas veces como test haya, es decir, 
    //Se ejecuta antes de ejecutar cada uno de los métodos etiquetados como @test
    @Before
    public void setUp() throws Exception{
            DatabaseOperation.CLEAN_INSERT.execute(connection, dataset);

    }

    //Este método se ejecuta Después de ejeucutar cada uno de los test
    //En este caso no tenemos que hacer nada porque la bd se pone en estado conocido al iniciar

    @After
    public void tearDown() throws Exception {
    }
	
	//Anotamos con @Test todos los test que queramos hacer, generalmnte uno
	//por método.Antes de iniciar el test la bd está en un estado conocido
	//es decir la base de datos del test está tal y como la ha configurado el dataset-dao.xml
	//tras su ejecución por parte de dbunit

        @Test
	public void testNuevoGrupo() {
		try {
			
			s.nuevoGrupo("8@gmail.com", "ed8", "Pepe", "España", "Alicante", "Sax", "03657", "C/ Mayor 6", "d","fg");
			List<Grupo> grupo = s.obtenerTodasGrupos();
			if (grupo.size() != 6) {//Hay que crear 5 editoriales de ejemplo
				fail("No se ha insertado el grupo");
			}
			for (Grupo j : grupo) {
				assertNotNull(j.getEmail());
                                assertNotNull(j.getPassword());
                                assertNotNull(j.getNombre());
                                assertNotNull(j.getPais());
                                assertNotNull(j.getProvincia());
                                assertNotNull(j.getLocalidad());
                                assertNotNull(j.getCodigopostal());
                                assertNotNull(j.getDireccion());
                                assertNotNull(j.getNombreResponsable());
                                assertNotNull(j.getCargoResponsable());
                                
			}

		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han insertado el grupo al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}

	}
        
        @Test
	public void testModificarGrupo(){
            try
            {
                
                s.modificaGrupo("11@email.com", "gmail.com", "ed8", "Pepe", "España", "Alicante", "Sax", "03657", "C/ Mayor 6", "d");
            
            } catch (DAOException e) {
                fail("TEST NO SUPERADO: No se ha modificado el grupo al producirse una excepcion: "
                        + e.getLocalizedMessage());
            } catch (ServicioException e) {
                fail("TEST NO SUPERADO: No se ha modificado el grupo al producirse una excepcion: "
                        + e.getLocalizedMessage());
            }
	}
        
        @Test
	public void testObtenerTodosGrupos() {
		try {

			List<Grupo> grupo = s.obtenerTodasGrupos();
			if (grupo.size() != 5) {
				fail("No se han obtenido todos los grupos");
			}
			for (Grupo j : grupo)  {
				assertNotNull(j.getEmail());
                                assertNotNull(j.getPassword());
                                assertNotNull(j.getNombre());
                                assertNotNull(j.getPais());
                                assertNotNull(j.getProvincia());
                                assertNotNull(j.getLocalidad());
                                assertNotNull(j.getCodigopostal());
                                assertNotNull(j.getDireccion());
                                assertNotNull(j.getNombreResponsable());
                                assertNotNull(j.getCargoResponsable());
			}
		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se han obtenido los grupos al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}
        @Test
	public void testEliminarGrupo() {
		try {
			//¿Que ocurre si decidimos eliminar un jugador que no existe o con id negativo?
			//según esta implementación no se hace nada, ya que no añadimos un test fail, para estos casos
			s.borraGrupo("6@email.com");
			List<Grupo> grupo = s.obtenerTodasGrupos();
			if (grupo.size() != 5) {
				fail("No se ha eliminado el grupo");
			}
			for (Grupo j : grupo)  {
				assertNotNull(j.getEmail());
                                assertNotNull(j.getPassword());
                                assertNotNull(j.getNombre());
                                assertNotNull(j.getPais());
                                assertNotNull(j.getProvincia());
                                assertNotNull(j.getLocalidad());
                                assertNotNull(j.getCodigopostal());
                                assertNotNull(j.getDireccion());
                                assertNotNull(j.getNombreResponsable());
                                assertNotNull(j.getCargoResponsable());
			}
			
		} catch (Exception e) {
			fail("TEST NO SUPERADO: No se ha eliminado el grupo al producirse una excepcion: "
					+ e.getLocalizedMessage());
		}
	}
	
        
}
